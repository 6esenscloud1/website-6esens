<?php namespace Tests\Repositories;

use App\Models\CategoryProject;
use App\Repositories\CategoryProjectRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CategoryProjectRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CategoryProjectRepository
     */
    protected $categoryProjectRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->categoryProjectRepo = \App::make(CategoryProjectRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_category_project()
    {
        $categoryProject = CategoryProject::factory()->make()->toArray();

        $createdCategoryProject = $this->categoryProjectRepo->create($categoryProject);

        $createdCategoryProject = $createdCategoryProject->toArray();
        $this->assertArrayHasKey('id', $createdCategoryProject);
        $this->assertNotNull($createdCategoryProject['id'], 'Created CategoryProject must have id specified');
        $this->assertNotNull(CategoryProject::find($createdCategoryProject['id']), 'CategoryProject with given id must be in DB');
        $this->assertModelData($categoryProject, $createdCategoryProject);
    }

    /**
     * @test read
     */
    public function test_read_category_project()
    {
        $categoryProject = CategoryProject::factory()->create();

        $dbCategoryProject = $this->categoryProjectRepo->find($categoryProject->id);

        $dbCategoryProject = $dbCategoryProject->toArray();
        $this->assertModelData($categoryProject->toArray(), $dbCategoryProject);
    }

    /**
     * @test update
     */
    public function test_update_category_project()
    {
        $categoryProject = CategoryProject::factory()->create();
        $fakeCategoryProject = CategoryProject::factory()->make()->toArray();

        $updatedCategoryProject = $this->categoryProjectRepo->update($fakeCategoryProject, $categoryProject->id);

        $this->assertModelData($fakeCategoryProject, $updatedCategoryProject->toArray());
        $dbCategoryProject = $this->categoryProjectRepo->find($categoryProject->id);
        $this->assertModelData($fakeCategoryProject, $dbCategoryProject->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_category_project()
    {
        $categoryProject = CategoryProject::factory()->create();

        $resp = $this->categoryProjectRepo->delete($categoryProject->id);

        $this->assertTrue($resp);
        $this->assertNull(CategoryProject::find($categoryProject->id), 'CategoryProject should not exist in DB');
    }
}
