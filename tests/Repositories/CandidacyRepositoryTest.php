<?php namespace Tests\Repositories;

use App\Models\Candidacy;
use App\Repositories\CandidacyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CandidacyRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CandidacyRepository
     */
    protected $candidacyRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->candidacyRepo = \App::make(CandidacyRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_candidacy()
    {
        $candidacy = Candidacy::factory()->make()->toArray();

        $createdCandidacy = $this->candidacyRepo->create($candidacy);

        $createdCandidacy = $createdCandidacy->toArray();
        $this->assertArrayHasKey('id', $createdCandidacy);
        $this->assertNotNull($createdCandidacy['id'], 'Created Candidacy must have id specified');
        $this->assertNotNull(Candidacy::find($createdCandidacy['id']), 'Candidacy with given id must be in DB');
        $this->assertModelData($candidacy, $createdCandidacy);
    }

    /**
     * @test read
     */
    public function test_read_candidacy()
    {
        $candidacy = Candidacy::factory()->create();

        $dbCandidacy = $this->candidacyRepo->find($candidacy->id);

        $dbCandidacy = $dbCandidacy->toArray();
        $this->assertModelData($candidacy->toArray(), $dbCandidacy);
    }

    /**
     * @test update
     */
    public function test_update_candidacy()
    {
        $candidacy = Candidacy::factory()->create();
        $fakeCandidacy = Candidacy::factory()->make()->toArray();

        $updatedCandidacy = $this->candidacyRepo->update($fakeCandidacy, $candidacy->id);

        $this->assertModelData($fakeCandidacy, $updatedCandidacy->toArray());
        $dbCandidacy = $this->candidacyRepo->find($candidacy->id);
        $this->assertModelData($fakeCandidacy, $dbCandidacy->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_candidacy()
    {
        $candidacy = Candidacy::factory()->create();

        $resp = $this->candidacyRepo->delete($candidacy->id);

        $this->assertTrue($resp);
        $this->assertNull(Candidacy::find($candidacy->id), 'Candidacy should not exist in DB');
    }
}
