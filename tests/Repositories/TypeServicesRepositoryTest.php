<?php namespace Tests\Repositories;

use App\Models\TypeServices;
use App\Repositories\TypeServicesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TypeServicesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TypeServicesRepository
     */
    protected $typeServicesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->typeServicesRepo = \App::make(TypeServicesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_type_services()
    {
        $typeServices = TypeServices::factory()->make()->toArray();

        $createdTypeServices = $this->typeServicesRepo->create($typeServices);

        $createdTypeServices = $createdTypeServices->toArray();
        $this->assertArrayHasKey('id', $createdTypeServices);
        $this->assertNotNull($createdTypeServices['id'], 'Created TypeServices must have id specified');
        $this->assertNotNull(TypeServices::find($createdTypeServices['id']), 'TypeServices with given id must be in DB');
        $this->assertModelData($typeServices, $createdTypeServices);
    }

    /**
     * @test read
     */
    public function test_read_type_services()
    {
        $typeServices = TypeServices::factory()->create();

        $dbTypeServices = $this->typeServicesRepo->find($typeServices->id);

        $dbTypeServices = $dbTypeServices->toArray();
        $this->assertModelData($typeServices->toArray(), $dbTypeServices);
    }

    /**
     * @test update
     */
    public function test_update_type_services()
    {
        $typeServices = TypeServices::factory()->create();
        $fakeTypeServices = TypeServices::factory()->make()->toArray();

        $updatedTypeServices = $this->typeServicesRepo->update($fakeTypeServices, $typeServices->id);

        $this->assertModelData($fakeTypeServices, $updatedTypeServices->toArray());
        $dbTypeServices = $this->typeServicesRepo->find($typeServices->id);
        $this->assertModelData($fakeTypeServices, $dbTypeServices->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_type_services()
    {
        $typeServices = TypeServices::factory()->create();

        $resp = $this->typeServicesRepo->delete($typeServices->id);

        $this->assertTrue($resp);
        $this->assertNull(TypeServices::find($typeServices->id), 'TypeServices should not exist in DB');
    }
}
