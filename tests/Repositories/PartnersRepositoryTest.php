<?php namespace Tests\Repositories;

use App\Models\Partners;
use App\Repositories\PartnersRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PartnersRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartnersRepository
     */
    protected $partnersRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->partnersRepo = \App::make(PartnersRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_partners()
    {
        $partners = Partners::factory()->make()->toArray();

        $createdPartners = $this->partnersRepo->create($partners);

        $createdPartners = $createdPartners->toArray();
        $this->assertArrayHasKey('id', $createdPartners);
        $this->assertNotNull($createdPartners['id'], 'Created Partners must have id specified');
        $this->assertNotNull(Partners::find($createdPartners['id']), 'Partners with given id must be in DB');
        $this->assertModelData($partners, $createdPartners);
    }

    /**
     * @test read
     */
    public function test_read_partners()
    {
        $partners = Partners::factory()->create();

        $dbPartners = $this->partnersRepo->find($partners->id);

        $dbPartners = $dbPartners->toArray();
        $this->assertModelData($partners->toArray(), $dbPartners);
    }

    /**
     * @test update
     */
    public function test_update_partners()
    {
        $partners = Partners::factory()->create();
        $fakePartners = Partners::factory()->make()->toArray();

        $updatedPartners = $this->partnersRepo->update($fakePartners, $partners->id);

        $this->assertModelData($fakePartners, $updatedPartners->toArray());
        $dbPartners = $this->partnersRepo->find($partners->id);
        $this->assertModelData($fakePartners, $dbPartners->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_partners()
    {
        $partners = Partners::factory()->create();

        $resp = $this->partnersRepo->delete($partners->id);

        $this->assertTrue($resp);
        $this->assertNull(Partners::find($partners->id), 'Partners should not exist in DB');
    }
}
