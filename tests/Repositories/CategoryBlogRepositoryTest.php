<?php namespace Tests\Repositories;

use App\Models\CategoryBlog;
use App\Repositories\CategoryBlogRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CategoryBlogRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CategoryBlogRepository
     */
    protected $categoryBlogRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->categoryBlogRepo = \App::make(CategoryBlogRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_category_blog()
    {
        $categoryBlog = CategoryBlog::factory()->make()->toArray();

        $createdCategoryBlog = $this->categoryBlogRepo->create($categoryBlog);

        $createdCategoryBlog = $createdCategoryBlog->toArray();
        $this->assertArrayHasKey('id', $createdCategoryBlog);
        $this->assertNotNull($createdCategoryBlog['id'], 'Created CategoryBlog must have id specified');
        $this->assertNotNull(CategoryBlog::find($createdCategoryBlog['id']), 'CategoryBlog with given id must be in DB');
        $this->assertModelData($categoryBlog, $createdCategoryBlog);
    }

    /**
     * @test read
     */
    public function test_read_category_blog()
    {
        $categoryBlog = CategoryBlog::factory()->create();

        $dbCategoryBlog = $this->categoryBlogRepo->find($categoryBlog->id);

        $dbCategoryBlog = $dbCategoryBlog->toArray();
        $this->assertModelData($categoryBlog->toArray(), $dbCategoryBlog);
    }

    /**
     * @test update
     */
    public function test_update_category_blog()
    {
        $categoryBlog = CategoryBlog::factory()->create();
        $fakeCategoryBlog = CategoryBlog::factory()->make()->toArray();

        $updatedCategoryBlog = $this->categoryBlogRepo->update($fakeCategoryBlog, $categoryBlog->id);

        $this->assertModelData($fakeCategoryBlog, $updatedCategoryBlog->toArray());
        $dbCategoryBlog = $this->categoryBlogRepo->find($categoryBlog->id);
        $this->assertModelData($fakeCategoryBlog, $dbCategoryBlog->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_category_blog()
    {
        $categoryBlog = CategoryBlog::factory()->create();

        $resp = $this->categoryBlogRepo->delete($categoryBlog->id);

        $this->assertTrue($resp);
        $this->assertNull(CategoryBlog::find($categoryBlog->id), 'CategoryBlog should not exist in DB');
    }
}
