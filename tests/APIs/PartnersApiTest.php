<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Partners;

class PartnersApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_partners()
    {
        $partners = Partners::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/partners', $partners
        );

        $this->assertApiResponse($partners);
    }

    /**
     * @test
     */
    public function test_read_partners()
    {
        $partners = Partners::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/partners/'.$partners->id
        );

        $this->assertApiResponse($partners->toArray());
    }

    /**
     * @test
     */
    public function test_update_partners()
    {
        $partners = Partners::factory()->create();
        $editedPartners = Partners::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/partners/'.$partners->id,
            $editedPartners
        );

        $this->assertApiResponse($editedPartners);
    }

    /**
     * @test
     */
    public function test_delete_partners()
    {
        $partners = Partners::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/partners/'.$partners->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/partners/'.$partners->id
        );

        $this->response->assertStatus(404);
    }
}
