<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Candidacy;

class CandidacyApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_candidacy()
    {
        $candidacy = Candidacy::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/candidacies', $candidacy
        );

        $this->assertApiResponse($candidacy);
    }

    /**
     * @test
     */
    public function test_read_candidacy()
    {
        $candidacy = Candidacy::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/candidacies/'.$candidacy->id
        );

        $this->assertApiResponse($candidacy->toArray());
    }

    /**
     * @test
     */
    public function test_update_candidacy()
    {
        $candidacy = Candidacy::factory()->create();
        $editedCandidacy = Candidacy::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/candidacies/'.$candidacy->id,
            $editedCandidacy
        );

        $this->assertApiResponse($editedCandidacy);
    }

    /**
     * @test
     */
    public function test_delete_candidacy()
    {
        $candidacy = Candidacy::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/candidacies/'.$candidacy->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/candidacies/'.$candidacy->id
        );

        $this->response->assertStatus(404);
    }
}
