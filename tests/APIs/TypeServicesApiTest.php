<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TypeServices;

class TypeServicesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_type_services()
    {
        $typeServices = TypeServices::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/type_services', $typeServices
        );

        $this->assertApiResponse($typeServices);
    }

    /**
     * @test
     */
    public function test_read_type_services()
    {
        $typeServices = TypeServices::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/type_services/'.$typeServices->id
        );

        $this->assertApiResponse($typeServices->toArray());
    }

    /**
     * @test
     */
    public function test_update_type_services()
    {
        $typeServices = TypeServices::factory()->create();
        $editedTypeServices = TypeServices::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/type_services/'.$typeServices->id,
            $editedTypeServices
        );

        $this->assertApiResponse($editedTypeServices);
    }

    /**
     * @test
     */
    public function test_delete_type_services()
    {
        $typeServices = TypeServices::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/type_services/'.$typeServices->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/type_services/'.$typeServices->id
        );

        $this->response->assertStatus(404);
    }
}
