<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CategoryBlog;

class CategoryBlogApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_category_blog()
    {
        $categoryBlog = CategoryBlog::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/category_blogs', $categoryBlog
        );

        $this->assertApiResponse($categoryBlog);
    }

    /**
     * @test
     */
    public function test_read_category_blog()
    {
        $categoryBlog = CategoryBlog::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/category_blogs/'.$categoryBlog->id
        );

        $this->assertApiResponse($categoryBlog->toArray());
    }

    /**
     * @test
     */
    public function test_update_category_blog()
    {
        $categoryBlog = CategoryBlog::factory()->create();
        $editedCategoryBlog = CategoryBlog::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/category_blogs/'.$categoryBlog->id,
            $editedCategoryBlog
        );

        $this->assertApiResponse($editedCategoryBlog);
    }

    /**
     * @test
     */
    public function test_delete_category_blog()
    {
        $categoryBlog = CategoryBlog::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/category_blogs/'.$categoryBlog->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/category_blogs/'.$categoryBlog->id
        );

        $this->response->assertStatus(404);
    }
}
