<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Project
 * @package App\Models
 * @version April 29, 2021, 4:57 pm UTC
 *
 */
class Project extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'projects';


    protected $dates = ['deleted_at'];

    protected $appends = ['CategoryProject'];



    public $fillable = [
        'title',
        'slug',
        'customer_campaign',
        'support',
        'description',
        'length_campaign',
        'image',
        'project_leader',
        'creative_director',
        'link_video',
        'medias',
        'category_project_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'category_project_id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'customer_campaign' => 'string',
        'length_campaign' => 'string',
        'link_video' => 'string',
        'creative_director' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function CategoryProject()
    {
        return $this->belongsTo(CategoryProject::class);
    }

    public function getCategoryProjectAttribute(){

        $categoryProjects = CategoryProject::find($this->category_project_id);

        return $categoryProjects;
    }



}
