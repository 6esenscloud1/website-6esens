<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class CategoryProject
 * @package App\Models
 * @version April 29, 2021, 4:58 pm UTC
 *
 */
class CategoryProject extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'category_projects';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function projects()
    {
        return $this->hasMany(Project::class,);
    }

}
