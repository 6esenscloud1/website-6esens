<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Slide
 * @package App\Models
 * @version April 29, 2021, 4:57 pm UTC
 *
 */
class Slide extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'slides';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'subtitle',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'subtitle' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
