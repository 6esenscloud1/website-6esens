<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class NewLetter
 * @package App\Models
 * @version April 29, 2021, 5:07 pm UTC
 *
 */
class NewLetter extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'new_letters';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
