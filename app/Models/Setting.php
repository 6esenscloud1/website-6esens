<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Setting
 * @package App\Models
 * @version April 29, 2021, 5:06 pm UTC
 *
 */
class Setting extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'settings';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'logo',
        'phone',
        'email',
        'geographical_location',
        'link_facebook',
        'link_twitter',
        'tagline',
        'link_linkedin',
        'link_youtube',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'logo' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'geographical_location' => 'string',
        'link_facebook' => 'string',
        'tagline' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
