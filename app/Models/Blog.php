<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Blog
 * @package App\Models
 * @version April 29, 2021, 5:19 pm UTC
 *
 */
class Blog extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'blogs';


    protected $dates = ['deleted_at'];

    protected $appends = ['categoryBlog'];



    public $fillable = [
        'title',
        'subtitle',
        'slug',
        'author',
        'publication_date',
        'medias',
        'description',
        'categoryblog_id',
        'image',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'categoryblog_id' => 'integer',
        'title' => 'string',
        'subtitle' => 'string',
        'image' => 'string',
        'author' => 'string',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function getCategoryBlogs()
    {
        return $this->belongsTo(CategoryBlog::class);
    }

    public function getCategoryBlogsAttribute(){

        $categoryBlog = CategoryBlog::find($this->categoryblog_id);

        return $categoryBlog;
    }


}
