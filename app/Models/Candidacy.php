<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Candidacy
 * @package App\Models
 * @version September 20, 2021, 11:20 am UTC
 *
 * @property string $name
 * @property string $birth_date
 * @property string $birthplace
 * @property string $desired_position
 * @property string $curriculum_vitae
 */
class Candidacy extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'candidacies';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'birth_date',
        'birthplace',
        'desired_position',
        'curriculum_vitae',
        'motivation_letter',
        'email',
        'phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'birth_date' => 'date',
        'birthplace' => 'string',
        'desired_position' => 'string',
        'curriculum_vitae' => 'string',
        'motivation_letter' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
