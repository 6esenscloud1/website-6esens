<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Team
 * @package App\Models
 * @version April 29, 2021, 5:08 pm UTC
 *
 */
class Team extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'teams';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'image',
        'description',
        'post',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'image' => 'string',
        'poste' => 'string',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
