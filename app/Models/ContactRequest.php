<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ContactRequest
 * @package App\Models
 * @version April 29, 2021, 5:05 pm UTC
 *
 */
class ContactRequest extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'contact_requests';


    protected $dates = ['deleted_at'];

    protected $appends = ['typeService'];



    public $fillable = [
        'name',
        'phone',
        'email',
        'company',
        'position_held',
        'servicio_id',
        'type_service_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'position_held' => 'string',
        'company' => 'string',
        'servicio_id' => 'integer',
        'type_service_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function typeService()
    {
        return $this->belongsTo(TypeServices::class);
    }

    public function getTypeServiceAttribute(){

        $typeService = TypeServices::find($this->type_service_id);

        return $typeService;
    }

}
