<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Servicio
 * @package App\Models
 * @version April 29, 2021, 5:02 pm UTC
 *
 */
class Servicio extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'servicios';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'subtitle',
        'description',
        'image',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'subtitle' => 'string',
        'description' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

}
