<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class TypeServices
 * @package App\Models
 * @version April 29, 2021, 5:05 pm UTC
 *
 */
class TypeServices extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'type_services';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'description',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function contactRequest()
    {
        return $this->hasMany(ContactRequest::class);
    }


}
