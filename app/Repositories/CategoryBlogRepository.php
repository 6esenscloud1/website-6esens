<?php

namespace App\Repositories;

use App\Models\CategoryBlog;
use App\Repositories\BaseRepository;

/**
 * Class CategoryBlogRepository
 * @package App\Repositories
 * @version April 29, 2021, 5:20 pm UTC
*/

class CategoryBlogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryBlog::class;
    }
}
