<?php

namespace App\Repositories;

use App\Models\NewLetter;
use App\Repositories\BaseRepository;

/**
 * Class NewLetterRepository
 * @package App\Repositories
 * @version April 29, 2021, 5:07 pm UTC
*/

class NewLetterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return NewLetter::class;
    }
}
