<?php

namespace App\Repositories;

use App\Models\Servicio;
use App\Repositories\BaseRepository;

/**
 * Class ServicioRepository
 * @package App\Repositories
 * @version April 29, 2021, 5:02 pm UTC
*/

class ServicioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Servicio::class;
    }
}
