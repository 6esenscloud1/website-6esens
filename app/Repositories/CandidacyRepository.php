<?php

namespace App\Repositories;

use App\Models\Candidacy;
use App\Repositories\BaseRepository;

/**
 * Class CandidacyRepository
 * @package App\Repositories
 * @version September 20, 2021, 11:20 am UTC
*/

class CandidacyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'birth_date',
        'birthplace',
        'desired_position',
        'curriculum_vitae'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Candidacy::class;
    }
}
