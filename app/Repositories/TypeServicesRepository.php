<?php

namespace App\Repositories;

use App\Models\TypeServices;
use App\Repositories\BaseRepository;

/**
 * Class TypeServicesRepository
 * @package App\Repositories
 * @version April 29, 2021, 5:05 pm UTC
*/

class TypeServicesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeServices::class;
    }
}
