<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryProjectRequest;
use App\Http\Requests\UpdateCategoryProjectRequest;
use App\Repositories\CategoryProjectRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\CategoryProject;
use Illuminate\Http\Request;
use Flash;
use Response;

class CategoryProjectController extends AppBaseController
{
    /** @var  CategoryProjectRepository */
    private $categoryProjectRepository;

    public function __construct(CategoryProjectRepository $categoryProjectRepo)
    {
        $this->categoryProjectRepository = $categoryProjectRepo;
    }

    /**
     * Display a listing of the CategoryProject.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $categoryProjects = CategoryProject::orderBy('id', 'desc')->paginate(10);

        // $categoryProjects = $this->categoryProjectRepository->all();

        return view('category_projects.index')
            ->with('categoryProjects', $categoryProjects);
    }

    /**
     * Show the form for creating a new CategoryProject.
     *
     * @return Response
     */
    public function create()
    {
        return view('category_projects.create');
    }

    /**
     * Store a newly created CategoryProject in storage.
     *
     * @param CreateCategoryProjectRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryProjectRequest $request)
    {
        $input = $request->all();

        $categoryProject = $this->categoryProjectRepository->create($input);

        Flash::success('Category Project saved successfully.');

        return redirect(route('categoryProjects.index'));
    }

    /**
     * Display the specified CategoryProject.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoryProject = $this->categoryProjectRepository->find($id);

        if (empty($categoryProject)) {
            Flash::error('Category Project not found');

            return redirect(route('categoryProjects.index'));
        }

        return view('category_projects.show')->with('categoryProject', $categoryProject);
    }

    /**
     * Show the form for editing the specified CategoryProject.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoryProject = $this->categoryProjectRepository->find($id);

        if (empty($categoryProject)) {
            Flash::error('Category Project not found');

            return redirect(route('categoryProjects.index'));
        }

        return view('category_projects.edit')->with('categoryProject', $categoryProject);
    }

    /**
     * Update the specified CategoryProject in storage.
     *
     * @param int $id
     * @param UpdateCategoryProjectRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryProjectRequest $request)
    {
        $categoryProject = $this->categoryProjectRepository->find($id);

        if (empty($categoryProject)) {
            Flash::error('Category Project not found');

            return redirect(route('categoryProjects.index'));
        }

        $categoryProject = $this->categoryProjectRepository->update($request->all(), $id);

        Flash::success('Category Project updated successfully.');

        return redirect(route('categoryProjects.index'));
    }

    /**
     * Remove the specified CategoryProject from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoryProject = $this->categoryProjectRepository->find($id);

        if (empty($categoryProject)) {
            Flash::error('Category Project not found');

            return redirect(route('categoryProjects.index'));
        }

        $this->categoryProjectRepository->delete($id);

        Flash::success('Category Project deleted successfully.');

        return redirect(route('categoryProjects.index'));
    }
}
