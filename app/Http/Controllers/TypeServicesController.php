<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTypeServicesRequest;
use App\Http\Requests\UpdateTypeServicesRequest;
use App\Repositories\TypeServicesRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\TypeServices;
use Illuminate\Http\Request;
use Flash;
use Response;

class TypeServicesController extends AppBaseController
{
    /** @var  TypeServicesRepository */
    private $typeServicesRepository;

    public function __construct(TypeServicesRepository $typeServicesRepo)
    {
        $this->typeServicesRepository = $typeServicesRepo;
    }

    /**
     * Display a listing of the TypeServices.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $typeServices = $this->typeServicesRepository->all();

        $typeServices = TypeServices::orderBy('id', 'desc')->paginate(10);

        return view('type_services.index')
            ->with('typeServices', $typeServices);
    }

    /**
     * Show the form for creating a new TypeServices.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_services.create');
    }

    /**
     * Store a newly created TypeServices in storage.
     *
     * @param CreateTypeServicesRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeServicesRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image'))
        {
            $destination_path = 'public/images';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }

        $typeServices = $this->typeServicesRepository->create($input);

        Flash::success('Type Services saved successfully.');

        return redirect(route('typeServices.index'));
    }

    /**
     * Display the specified TypeServices.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeServices = $this->typeServicesRepository->find($id);

        if (empty($typeServices)) {
            Flash::error('Type Services not found');

            return redirect(route('typeServices.index'));
        }

        return view('type_services.show')->with('typeServices', $typeServices);
    }

    /**
     * Show the form for editing the specified TypeServices.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeServices = $this->typeServicesRepository->find($id);

        if (empty($typeServices)) {
            Flash::error('Type Services not found');

            return redirect(route('typeServices.index'));
        }

        return view('type_services.edit')->with('typeServices', $typeServices);
    }

    /**
     * Update the specified TypeServices in storage.
     *
     * @param int $id
     * @param UpdateTypeServicesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeServicesRequest $request)
    {
        $typeServices = $this->typeServicesRepository->find($id);

        if (empty($typeServices)) {
            Flash::error('Type Services not found');

            return redirect(route('typeServices.index'));
        }

        $typeServices = $this->typeServicesRepository->update($request->all(), $id);

        Flash::success('Type Services updated successfully.');

        return redirect(route('typeServices.index'));
    }

    /**
     * Remove the specified TypeServices from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeServices = $this->typeServicesRepository->find($id);

        if (empty($typeServices)) {
            Flash::error('Type Services not found');

            return redirect(route('typeServices.index'));
        }

        $this->typeServicesRepository->delete($id);

        Flash::success('Type Services deleted successfully.');

        return redirect(route('typeServices.index'));
    }
}
