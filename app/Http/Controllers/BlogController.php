<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBlogRequest;
use App\Http\Requests\UpdateBlogRequest;
use App\Repositories\BlogRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Blog;
use App\Models\CategoryBlog;
use Illuminate\Http\Request;
use Flash;
use Response;

class BlogController extends AppBaseController
{
    /** @var  BlogRepository */
    private $blogRepository;

    public function __construct(BlogRepository $blogRepo)
    {
        $this->blogRepository = $blogRepo;
    }

    /**
     * Display a listing of the Blog.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $blogs = Blog::orderBy('id', 'desc')->paginate(10);
        // $blogs = $this->blogRepository->all();

        return view('blogs.index')
            ->with('blogs', $blogs);
    }

    /**
     * Show the form for creating a new Blog.
     *
     * @return Response
     */
    public function create()
    {
        $categoryBlogs = CategoryBlog::all();

        return view('blogs.create', compact('categoryBlogs'));
    }

    /**
     * Store a newly created Blog in storage.
     *
     * @param CreateBlogRequest $request
     *
     * @return Response
     */
    public function store(CreateBlogRequest $request)
    {
        $input = $request->all();
        // dd($input);


        if($request->has("category_blogs_id")){
            if( is_null($request->get("category_blogs_id")) || empty($request->get("category_blogs_id"))){
                return $this->sendError('category_blogs_id is Null or Empty');
            }
        }else{
            return $this->sendError('category_blogs_id is Required', 400);
        }

        $categoryBlogs = CategoryBlog::find($request->get("category_blogs_id"));
        // dd($categoryBlogs);
        // dd(gettype($categoryBlogs));

        if ($request->hasFile('image'))
        {
            $destination_path = 'public/images';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }


        if ($request->file('medias')) {

            $pictures = [];
            $image = $request->file('medias');
            if ($image) {
                for ($i = 0; $i < sizeof($image); $i++) {
                    ${'image' . ($i)} = $image[$i]->store('storage/images', ['disk' => 'public']);
                    ${'picture' . ($i)} = ${'image' . ($i)} ?? null;
                    array_push($pictures, ${'picture' . ($i)});
                }
                // dd($pictures);
            }

        } else {
            $pictures = null;
        }

        $blogImageLink = json_encode($pictures);

        $input["medias"] = $blogImageLink;

        $input["categoryblog_id"] = $categoryBlogs->id;

        $blog = $this->blogRepository->create($input);

        Flash::success('Blog saved successfully.');

        return redirect(route('blogs.index'));
    }

    /**
     * Display the specified Blog.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $blog = $this->blogRepository->find($id);

        if (empty($blog)) {
            Flash::error('Blog not found');

            return redirect(route('blogs.index'));
        }

        return view('blogs.show')->with('blog', $blog);
    }

    /**
     * Show the form for editing the specified Blog.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $blog = $this->blogRepository->find($id);

        if (empty($blog)) {
            Flash::error('Blog not found');

            return redirect(route('blogs.index'));
        }

        $categoryBlogs = CategoryBlog::all();

        return view('blogs.edit')
               ->with('blog', $blog)
               ->with('categoryBlogs', $categoryBlogs);
    }

    /**
     * Update the specified Blog in storage.
     *
     * @param int $id
     * @param UpdateBlogRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBlogRequest $request)
    {
        $blog = $this->blogRepository->find($id);

        if (empty($blog)) {
            Flash::error('Blog not found');

            return redirect(route('blogs.index'));
        }

        $input = $request->all();

        if ($request->hasFile('image'))
        {
            $destination_path = 'public/images';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }


        if ($request->file('medias')) {

            $pictures = [];
            $image = $request->file('medias');
            if ($image) {
                for ($i = 0; $i < sizeof($image); $i++) {
                    ${'image' . ($i)} = $image[$i]->store('public/images', ['disk' => 'public']);
                    ${'picture' . ($i)} = ${'image' . ($i)} ?? null;
                    array_push($pictures, ${'picture' . ($i)});
                }
                // dd($pictures);
            }

        } else {
            $pictures = null;
        }

        $blogImageLink = json_encode($pictures);

        $input["medias"] = $blogImageLink;

        $input['publication_date'] = $blog->publication_date;
        $input['description'] = $blog->description;

        $blog = $this->blogRepository->update($request->all(), $id);

        Flash::success('Blog updated successfully.');

        return redirect(route('blogs.index'));
    }

    /**
     * Remove the specified Blog from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $blog = $this->blogRepository->find($id);

        if (empty($blog)) {
            Flash::error('Blog not found');

            return redirect(route('blogs.index'));
        }

        $this->blogRepository->delete($id);

        Flash::success('Blog deleted successfully.');

        return redirect(route('blogs.index'));
    }
}
