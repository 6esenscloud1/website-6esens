<?php

namespace App\Http\Controllers;


use App\Models\CategoryProject;
use App\Models\Project;
use App\Models\Servicio;
use App\Models\Team;
use App\Models\TypeServices;
use Illuminate\Http\Request;

class FrontOfficeController extends Controller
{
    public function HomePage()
    {
        $projects = Project::all();

        $categoryProjects = CategoryProject::with('projects')->get();

        return view()->exists('Customer_FrontOffice.index') ? view('Customer_FrontOffice.index', compact('projects','categoryProjects')) : '';
    }



    public function Contact()
    {
        $typeServices = TypeServices::all();

        $servicios = Servicio::all();

        return view('Customer_FrontOffice.collaborate', compact('typeServices', 'servicios'));
    }

    public function About()
    {
        $teams = Team::all();

        return view('Customer_FrontOffice.about', compact('teams'));
    }

    public function Services()
    {
        return view('Customer_FrontOffice.servicios');
    }

}
