<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\ContactRequest;
use App\Models\NewLetter;
use App\Models\Project;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $projects = Project::all();
        $countProject = count($projects);

        $newsletters = NewLetter::all();
        $countNewsletter = count($newsletters);

        $contactRequests = ContactRequest::all();
        $countContactRequest = count($contactRequests);

        $blogs = Blog::all();
        $countBlog = count($blogs);

        return view('home', compact('countProject', 'countNewsletter', 'countContactRequest', 'countNewsletter', 'countBlog', 'contactRequests'));
    }
}
