<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCandidacyAPIRequest;
use App\Http\Requests\API\UpdateCandidacyAPIRequest;
use App\Models\Candidacy;
use App\Repositories\CandidacyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CandidacyController
 * @package App\Http\Controllers\API
 */

class CandidacyAPIController extends AppBaseController
{
    /** @var  CandidacyRepository */
    private $candidacyRepository;

    public function __construct(CandidacyRepository $candidacyRepo)
    {
        $this->candidacyRepository = $candidacyRepo;
    }

    /**
     * Display a listing of the Candidacy.
     * GET|HEAD /candidacies
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $candidacies = $this->candidacyRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($candidacies->toArray(), 'Candidacies retrieved successfully');
    }

    /**
     * Store a newly created Candidacy in storage.
     * POST /candidacies
     *
     * @param CreateCandidacyAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCandidacyAPIRequest $request)
    {
        $input = $request->all();

        $candidacy = $this->candidacyRepository->create($input);

        return $this->sendResponse($candidacy->toArray(), 'Candidacy saved successfully');
    }

    /**
     * Display the specified Candidacy.
     * GET|HEAD /candidacies/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Candidacy $candidacy */
        $candidacy = $this->candidacyRepository->find($id);

        if (empty($candidacy)) {
            return $this->sendError('Candidacy not found');
        }

        return $this->sendResponse($candidacy->toArray(), 'Candidacy retrieved successfully');
    }

    /**
     * Update the specified Candidacy in storage.
     * PUT/PATCH /candidacies/{id}
     *
     * @param int $id
     * @param UpdateCandidacyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCandidacyAPIRequest $request)
    {
        $input = $request->all();

        /** @var Candidacy $candidacy */
        $candidacy = $this->candidacyRepository->find($id);

        if (empty($candidacy)) {
            return $this->sendError('Candidacy not found');
        }

        $candidacy = $this->candidacyRepository->update($input, $id);

        return $this->sendResponse($candidacy->toArray(), 'Candidacy updated successfully');
    }

    /**
     * Remove the specified Candidacy from storage.
     * DELETE /candidacies/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Candidacy $candidacy */
        $candidacy = $this->candidacyRepository->find($id);

        if (empty($candidacy)) {
            return $this->sendError('Candidacy not found');
        }

        $candidacy->delete();

        return $this->sendSuccess('Candidacy deleted successfully');
    }
}
