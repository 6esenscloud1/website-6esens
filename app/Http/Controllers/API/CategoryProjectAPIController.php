<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategoryProjectAPIRequest;
use App\Http\Requests\API\UpdateCategoryProjectAPIRequest;
use App\Models\CategoryProject;
use App\Repositories\CategoryProjectRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CategoryProjectController
 * @package App\Http\Controllers\API
 */

class CategoryProjectAPIController extends AppBaseController
{
    /** @var  CategoryProjectRepository */
    private $categoryProjectRepository;

    public function __construct(CategoryProjectRepository $categoryProjectRepo)
    {
        $this->categoryProjectRepository = $categoryProjectRepo;
    }

    /**
     * Display a listing of the CategoryProject.
     * GET|HEAD /categoryProjects
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $categoryProjects = $this->categoryProjectRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($categoryProjects->toArray(), 'Category Projects retrieved successfully');
    }

    /**
     * Store a newly created CategoryProject in storage.
     * POST /categoryProjects
     *
     * @param CreateCategoryProjectAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryProjectAPIRequest $request)
    {
        $input = $request->all();

        $categoryProject = $this->categoryProjectRepository->create($input);

        return $this->sendResponse($categoryProject->toArray(), 'Category Project saved successfully');
    }

    /**
     * Display the specified CategoryProject.
     * GET|HEAD /categoryProjects/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CategoryProject $categoryProject */
        $categoryProject = $this->categoryProjectRepository->find($id);

        if (empty($categoryProject)) {
            return $this->sendError('Category Project not found');
        }

        return $this->sendResponse($categoryProject->toArray(), 'Category Project retrieved successfully');
    }

    /**
     * Update the specified CategoryProject in storage.
     * PUT/PATCH /categoryProjects/{id}
     *
     * @param int $id
     * @param UpdateCategoryProjectAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryProjectAPIRequest $request)
    {
        $input = $request->all();

        /** @var CategoryProject $categoryProject */
        $categoryProject = $this->categoryProjectRepository->find($id);

        if (empty($categoryProject)) {
            return $this->sendError('Category Project not found');
        }

        $categoryProject = $this->categoryProjectRepository->update($input, $id);

        return $this->sendResponse($categoryProject->toArray(), 'CategoryProject updated successfully');
    }

    /**
     * Remove the specified CategoryProject from storage.
     * DELETE /categoryProjects/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CategoryProject $categoryProject */
        $categoryProject = $this->categoryProjectRepository->find($id);

        if (empty($categoryProject)) {
            return $this->sendError('Category Project not found');
        }

        $categoryProject->delete();

        return $this->sendSuccess('Category Project deleted successfully');
    }
}
