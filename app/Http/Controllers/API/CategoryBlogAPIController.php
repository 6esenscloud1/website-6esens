<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategoryBlogAPIRequest;
use App\Http\Requests\API\UpdateCategoryBlogAPIRequest;
use App\Models\CategoryBlog;
use App\Repositories\CategoryBlogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CategoryBlogController
 * @package App\Http\Controllers\API
 */

class CategoryBlogAPIController extends AppBaseController
{
    /** @var  CategoryBlogRepository */
    private $categoryBlogRepository;

    public function __construct(CategoryBlogRepository $categoryBlogRepo)
    {
        $this->categoryBlogRepository = $categoryBlogRepo;
    }

    /**
     * Display a listing of the CategoryBlog.
     * GET|HEAD /categoryBlogs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $categoryBlogs = $this->categoryBlogRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($categoryBlogs->toArray(), 'Category Blogs retrieved successfully');
    }

    /**
     * Store a newly created CategoryBlog in storage.
     * POST /categoryBlogs
     *
     * @param CreateCategoryBlogAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryBlogAPIRequest $request)
    {
        $input = $request->all();

        $categoryBlog = $this->categoryBlogRepository->create($input);

        return $this->sendResponse($categoryBlog->toArray(), 'Category Blog saved successfully');
    }

    /**
     * Display the specified CategoryBlog.
     * GET|HEAD /categoryBlogs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CategoryBlog $categoryBlog */
        $categoryBlog = $this->categoryBlogRepository->find($id);

        if (empty($categoryBlog)) {
            return $this->sendError('Category Blog not found');
        }

        return $this->sendResponse($categoryBlog->toArray(), 'Category Blog retrieved successfully');
    }

    /**
     * Update the specified CategoryBlog in storage.
     * PUT/PATCH /categoryBlogs/{id}
     *
     * @param int $id
     * @param UpdateCategoryBlogAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryBlogAPIRequest $request)
    {
        $input = $request->all();

        /** @var CategoryBlog $categoryBlog */
        $categoryBlog = $this->categoryBlogRepository->find($id);

        if (empty($categoryBlog)) {
            return $this->sendError('Category Blog not found');
        }

        $categoryBlog = $this->categoryBlogRepository->update($input, $id);

        return $this->sendResponse($categoryBlog->toArray(), 'CategoryBlog updated successfully');
    }

    /**
     * Remove the specified CategoryBlog from storage.
     * DELETE /categoryBlogs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CategoryBlog $categoryBlog */
        $categoryBlog = $this->categoryBlogRepository->find($id);

        if (empty($categoryBlog)) {
            return $this->sendError('Category Blog not found');
        }

        $categoryBlog->delete();

        return $this->sendSuccess('Category Blog deleted successfully');
    }
}
