<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTypeServicesAPIRequest;
use App\Http\Requests\API\UpdateTypeServicesAPIRequest;
use App\Models\TypeServices;
use App\Repositories\TypeServicesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TypeServicesController
 * @package App\Http\Controllers\API
 */

class TypeServicesAPIController extends AppBaseController
{
    /** @var  TypeServicesRepository */
    private $typeServicesRepository;

    public function __construct(TypeServicesRepository $typeServicesRepo)
    {
        $this->typeServicesRepository = $typeServicesRepo;
    }

    /**
     * Display a listing of the TypeServices.
     * GET|HEAD /typeServices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $typeServices = $this->typeServicesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($typeServices->toArray(), 'Type Services retrieved successfully');
    }

    /**
     * Store a newly created TypeServices in storage.
     * POST /typeServices
     *
     * @param CreateTypeServicesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeServicesAPIRequest $request)
    {
        $input = $request->all();

        $typeServices = $this->typeServicesRepository->create($input);

        return $this->sendResponse($typeServices->toArray(), 'Type Services saved successfully');
    }

    /**
     * Display the specified TypeServices.
     * GET|HEAD /typeServices/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TypeServices $typeServices */
        $typeServices = $this->typeServicesRepository->find($id);

        if (empty($typeServices)) {
            return $this->sendError('Type Services not found');
        }

        return $this->sendResponse($typeServices->toArray(), 'Type Services retrieved successfully');
    }

    /**
     * Update the specified TypeServices in storage.
     * PUT/PATCH /typeServices/{id}
     *
     * @param int $id
     * @param UpdateTypeServicesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeServicesAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypeServices $typeServices */
        $typeServices = $this->typeServicesRepository->find($id);

        if (empty($typeServices)) {
            return $this->sendError('Type Services not found');
        }

        $typeServices = $this->typeServicesRepository->update($input, $id);

        return $this->sendResponse($typeServices->toArray(), 'TypeServices updated successfully');
    }

    /**
     * Remove the specified TypeServices from storage.
     * DELETE /typeServices/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TypeServices $typeServices */
        $typeServices = $this->typeServicesRepository->find($id);

        if (empty($typeServices)) {
            return $this->sendError('Type Services not found');
        }

        $typeServices->delete();

        return $this->sendSuccess('Type Services deleted successfully');
    }
}
