<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartnersAPIRequest;
use App\Http\Requests\API\UpdatePartnersAPIRequest;
use App\Models\Partners;
use App\Repositories\PartnersRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PartnersController
 * @package App\Http\Controllers\API
 */

class PartnersAPIController extends AppBaseController
{
    /** @var  PartnersRepository */
    private $partnersRepository;

    public function __construct(PartnersRepository $partnersRepo)
    {
        $this->partnersRepository = $partnersRepo;
    }

    /**
     * Display a listing of the Partners.
     * GET|HEAD /partners
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $partners = $this->partnersRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($partners->toArray(), 'Partners retrieved successfully');
    }

    /**
     * Store a newly created Partners in storage.
     * POST /partners
     *
     * @param CreatePartnersAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartnersAPIRequest $request)
    {
        $input = $request->all();

        $partners = $this->partnersRepository->create($input);

        return $this->sendResponse($partners->toArray(), 'Partners saved successfully');
    }

    /**
     * Display the specified Partners.
     * GET|HEAD /partners/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Partners $partners */
        $partners = $this->partnersRepository->find($id);

        if (empty($partners)) {
            return $this->sendError('Partners not found');
        }

        return $this->sendResponse($partners->toArray(), 'Partners retrieved successfully');
    }

    /**
     * Update the specified Partners in storage.
     * PUT/PATCH /partners/{id}
     *
     * @param int $id
     * @param UpdatePartnersAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartnersAPIRequest $request)
    {
        $input = $request->all();

        /** @var Partners $partners */
        $partners = $this->partnersRepository->find($id);

        if (empty($partners)) {
            return $this->sendError('Partners not found');
        }

        $partners = $this->partnersRepository->update($input, $id);

        return $this->sendResponse($partners->toArray(), 'Partners updated successfully');
    }

    /**
     * Remove the specified Partners from storage.
     * DELETE /partners/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Partners $partners */
        $partners = $this->partnersRepository->find($id);

        if (empty($partners)) {
            return $this->sendError('Partners not found');
        }

        $partners->delete();

        return $this->sendSuccess('Partners deleted successfully');
    }
}
