<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateContactRequestRequest;
use App\Http\Requests\UpdateContactRequestRequest;
use App\Repositories\ContactRequestRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\ContactRequest;
use App\Models\Servicio;
use App\Models\TypeServices;
use Illuminate\Http\Request;
use Flash;
use Response;

class ContactRequestController extends AppBaseController
{
    /** @var  ContactRequestRepository */
    private $contactRequestRepository;

    public function __construct(ContactRequestRepository $contactRequestRepo)
    {
        $this->contactRequestRepository = $contactRequestRepo;
    }

    /**
     * Display a listing of the ContactRequest.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $contactRequests = $this->contactRequestRepository->all();

        $contactRequests = ContactRequest::orderBy('id', 'desc')->paginate(10);

        return view('contact_requests.index')
            ->with('contactRequests', $contactRequests);
    }

    /**
     * Show the form for creating a new ContactRequest.
     *
     * @return Response
     */
    public function create()
    {
        return view('contact_requests.create');
    }

    /**
     * Store a newly created ContactRequest in storage.
     *
     * @param CreateContactRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateContactRequestRequest $request)
    {
        $input = $request->all();

        $contactRequest = $this->contactRequestRepository->create($input);

        Flash::success('Contact Request saved successfully.');

        return redirect(route('contactRequests.index'));
    }

    /**
     * Display the specified ContactRequest.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contactRequest = $this->contactRequestRepository->find($id);

        if (empty($contactRequest)) {
            Flash::error('Contact Request not found');

            return redirect(route('contactRequests.index'));
        }

        return view('contact_requests.show')->with('contactRequest', $contactRequest);
    }

    /**
     * Show the form for editing the specified ContactRequest.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $contactRequest = $this->contactRequestRepository->find($id);

        if (empty($contactRequest)) {
            Flash::error('Contact Request not found');

            return redirect(route('contactRequests.index'));
        }

        return view('contact_requests.edit')->with('contactRequest', $contactRequest);
    }

    /**
     * Update the specified ContactRequest in storage.
     *
     * @param int $id
     * @param UpdateContactRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContactRequestRequest $request)
    {
        $contactRequest = $this->contactRequestRepository->find($id);

        if (empty($contactRequest)) {
            Flash::error('Contact Request not found');

            return redirect(route('contactRequests.index'));
        }

        $contactRequest = $this->contactRequestRepository->update($request->all(), $id);

        Flash::success('Contact Request updated successfully.');

        return redirect(route('contactRequests.index'));
    }

    /**
     * Remove the specified ContactRequest from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contactRequest = $this->contactRequestRepository->find($id);

        if (empty($contactRequest)) {
            Flash::error('Contact Request not found');

            return redirect(route('contactRequests.index'));
        }

        $this->contactRequestRepository->delete($id);

        Flash::success('Contact Request deleted successfully.');

        return redirect(route('contactRequests.index'));
    }


    public function addContactRequest(CreateContactRequestRequest $request)
    {

        // $typeServices = TypeServices::find($request->get("type_services_id"));
        // dd($typeServices);

        $data = $request->validate([
            'name' => 'required',
            'email' => 'required | email',
            'phone' => 'required',
            'company' => 'required',
            'position_held' => 'required',
        ]);
        // dd($data);

        $contactRequest = new ContactRequest();
        $contactRequest->name = $data['name'];
        $contactRequest->phone = $data['phone'];
        $contactRequest->email = $data['email'];
        $contactRequest->subject = $data['company'];
        $contactRequest->message = $data['position_held'];
        // dd($contactRequest);
        // $contactRequest->type_service_id = $typeServices->id;

        $contactRequest->save();




        session()->flash('success', "Message envoyé enregistrée avec success");

        session(['name'=> $contactRequest->name]);

        return redirect()->back();
    }
}
