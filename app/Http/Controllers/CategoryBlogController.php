<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryBlogRequest;
use App\Http\Requests\UpdateCategoryBlogRequest;
use App\Repositories\CategoryBlogRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\CategoryBlog;
use Illuminate\Http\Request;
use Flash;
use Response;

class CategoryBlogController extends AppBaseController
{
    /** @var  CategoryBlogRepository */
    private $categoryBlogRepository;

    public function __construct(CategoryBlogRepository $categoryBlogRepo)
    {
        $this->categoryBlogRepository = $categoryBlogRepo;
    }

    /**
     * Display a listing of the CategoryBlog.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $categoryBlogs = CategoryBlog::orderBy('id', 'desc')->paginate(10);

        // $categoryBlogs = $this->categoryBlogRepository->all();

        return view('category_blogs.index')
            ->with('categoryBlogs', $categoryBlogs);
    }

    /**
     * Show the form for creating a new CategoryBlog.
     *
     * @return Response
     */
    public function create()
    {
        return view('category_blogs.create');
    }

    /**
     * Store a newly created CategoryBlog in storage.
     *
     * @param CreateCategoryBlogRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryBlogRequest $request)
    {
        $input = $request->all();

        $categoryBlog = $this->categoryBlogRepository->create($input);

        Flash::success('Category Blog saved successfully.');

        return redirect(route('categoryBlogs.index'));
    }

    /**
     * Display the specified CategoryBlog.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoryBlog = $this->categoryBlogRepository->find($id);

        if (empty($categoryBlog)) {
            Flash::error('Category Blog not found');

            return redirect(route('categoryBlogs.index'));
        }

        return view('category_blogs.show')->with('categoryBlog', $categoryBlog);
    }

    /**
     * Show the form for editing the specified CategoryBlog.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoryBlog = $this->categoryBlogRepository->find($id);

        if (empty($categoryBlog)) {
            Flash::error('Category Blog not found');

            return redirect(route('categoryBlogs.index'));
        }

        return view('category_blogs.edit')->with('categoryBlog', $categoryBlog);
    }

    /**
     * Update the specified CategoryBlog in storage.
     *
     * @param int $id
     * @param UpdateCategoryBlogRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryBlogRequest $request)
    {
        $categoryBlog = $this->categoryBlogRepository->find($id);

        if (empty($categoryBlog)) {
            Flash::error('Category Blog not found');

            return redirect(route('categoryBlogs.index'));
        }

        $categoryBlog = $this->categoryBlogRepository->update($request->all(), $id);

        Flash::success('Category Blog updated successfully.');

        return redirect(route('categoryBlogs.index'));
    }

    /**
     * Remove the specified CategoryBlog from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoryBlog = $this->categoryBlogRepository->find($id);

        if (empty($categoryBlog)) {
            Flash::error('Category Blog not found');

            return redirect(route('categoryBlogs.index'));
        }

        $this->categoryBlogRepository->delete($id);

        Flash::success('Category Blog deleted successfully.');

        return redirect(route('categoryBlogs.index'));
    }
}
