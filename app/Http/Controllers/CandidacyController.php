<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCandidacyRequest;
use App\Http\Requests\UpdateCandidacyRequest;
use App\Repositories\CandidacyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CandidacyController extends AppBaseController
{
    /** @var  CandidacyRepository */
    private $candidacyRepository;

    public function __construct(CandidacyRepository $candidacyRepo)
    {
        $this->candidacyRepository = $candidacyRepo;
    }

    /**
     * Display a listing of the Candidacy.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $candidacies = $this->candidacyRepository->all();

        return view('candidacies.index')
            ->with('candidacies', $candidacies);
    }

    /**
     * Show the form for creating a new Candidacy.
     *
     * @return Response
     */
    public function create()
    {
        return view('candidacies.create');
    }

    /**
     * Store a newly created Candidacy in storage.
     *
     * @param CreateCandidacyRequest $request
     *
     * @return Response
     */
    public function store(CreateCandidacyRequest $request)
    {
        $input = $request->all();

        $candidacy = $this->candidacyRepository->create($input);

        Flash::success('Candidacy saved successfully.');

        return redirect(route('candidacies.index'));
    }

    /**
     * Display the specified Candidacy.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $candidacy = $this->candidacyRepository->find($id);

        if (empty($candidacy)) {
            Flash::error('Candidacy not found');

            return redirect(route('candidacies.index'));
        }

        return view('candidacies.show')->with('candidacy', $candidacy);
    }

    /**
     * Show the form for editing the specified Candidacy.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $candidacy = $this->candidacyRepository->find($id);

        if (empty($candidacy)) {
            Flash::error('Candidacy not found');

            return redirect(route('candidacies.index'));
        }

        return view('candidacies.edit')->with('candidacy', $candidacy);
    }

    /**
     * Update the specified Candidacy in storage.
     *
     * @param int $id
     * @param UpdateCandidacyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCandidacyRequest $request)
    {
        $candidacy = $this->candidacyRepository->find($id);

        if (empty($candidacy)) {
            Flash::error('Candidacy not found');

            return redirect(route('candidacies.index'));
        }

        $candidacy = $this->candidacyRepository->update($request->all(), $id);

        Flash::success('Candidacy updated successfully.');

        return redirect(route('candidacies.index'));
    }

    /**
     * Remove the specified Candidacy from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $candidacy = $this->candidacyRepository->find($id);

        if (empty($candidacy)) {
            Flash::error('Candidacy not found');

            return redirect(route('candidacies.index'));
        }

        $this->candidacyRepository->delete($id);

        Flash::success('Candidacy deleted successfully.');

        return redirect(route('candidacies.index'));
    }
}
