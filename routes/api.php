<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::resource('slides', App\Http\Controllers\API\SlideAPIController::class);

Route::resource('projects', App\Http\Controllers\API\ProjectAPIController::class);

Route::resource('category_projects', App\Http\Controllers\API\CategoryProjectAPIController::class);

Route::resource('servicios', App\Http\Controllers\API\ServicioAPIController::class);

Route::resource('type_services', App\Http\Controllers\API\TypeServicesAPIController::class);

Route::resource('contact_requests', App\Http\Controllers\API\ContactRequestAPIController::class);

Route::resource('settings', App\Http\Controllers\API\SettingAPIController::class);

Route::resource('new_letters', App\Http\Controllers\API\NewLetterAPIController::class);

Route::resource('teams', App\Http\Controllers\API\TeamAPIController::class);

Route::resource('blogs', App\Http\Controllers\API\BlogAPIController::class);

Route::resource('category_blogs', App\Http\Controllers\API\CategoryBlogAPIController::class);

Route::resource('users', App\Http\Controllers\API\UserAPIController::class);


Route::resource('partners', App\Http\Controllers\API\PartnersAPIController::class);


Route::resource('candidacies', App\Http\Controllers\API\CandidacyAPIController::class);
