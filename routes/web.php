<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Artisan::call('storage:link');

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', [App\Http\Controllers\FrontOfficeController::class, 'HomePage']);

Route::get('/contact', [App\Http\Controllers\FrontOfficeController::class, "Contact"])->name('contact');

Route::get('/about', [App\Http\Controllers\FrontOfficeController::class, "About"])->name('about');

Route::get('/services', [App\Http\Controllers\FrontOfficeController::class, "Services"])->name('services');

// Route::get('/detailproject', [App\Http\Controllers\FrontOfficeController::class, "showProject"])->name('detailproject');

Route::post('/contacter', [App\Http\Controllers\ContactRequestController::class, "addContactRequest"])->name('contacter');

// Route::post('/footer', [App\Http\Controllers\NewLetterController::class, "addEmail"])->name('footer');


// Route::get('/portfolios', [App\Http\Controllers\FrontOfficeController::class, "IndexProjects"])->name('portfolios');

Route::resource('slides', App\Http\Controllers\SlideController::class);

Route::resource('projects', App\Http\Controllers\ProjectController::class);

Route::resource('categoryProjects', App\Http\Controllers\CategoryProjectController::class);

Route::resource('servicios', App\Http\Controllers\ServicioController::class);

Route::resource('typeServices', App\Http\Controllers\TypeServicesController::class);

Route::resource('contactRequests', App\Http\Controllers\ContactRequestController::class);

Route::post('ckeditor/upload', [App\Http\Controllers\CKEditorController::class, "upload"])->name('upload');

Route::resource('settings', App\Http\Controllers\SettingController::class);

Route::resource('newLetters', App\Http\Controllers\NewLetterController::class);

Route::resource('teams', App\Http\Controllers\TeamController::class);

Route::resource('blogs', App\Http\Controllers\BlogController::class);

Route::resource('categoryBlogs', App\Http\Controllers\CategoryBlogController::class);


Route::resource('users', App\Http\Controllers\UserController::class);


Route::resource('partners', App\Http\Controllers\PartnersController::class);


Route::resource('candidacies', App\Http\Controllers\CandidacyController::class);
