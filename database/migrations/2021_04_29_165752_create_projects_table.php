<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('customer_campaign')->nullable();
            $table->text('description')->nullable();
            $table->string('support')->nullable();
            $table->string('image')->nullable();
            $table->string('length_campaign')->nullable();
            $table->string('project_leader')->nullable();
            $table->string('creative_director')->nullable();
            $table->string('link_video')->nullable();
            $table->text('medias')->nullable();
            $table->foreignId('category_project_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
