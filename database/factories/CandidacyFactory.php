<?php

namespace Database\Factories;

use App\Models\Candidacy;
use Illuminate\Database\Eloquent\Factories\Factory;

class CandidacyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Candidacy::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'birth_date' => $this->faker->word,
        'birthplace' => $this->faker->word,
        'desired_position' => $this->faker->word,
        'curriculum_vitae' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
