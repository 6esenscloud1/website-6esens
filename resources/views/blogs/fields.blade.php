<!-- Title Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('title', 'Titre de l&rsquo;Article:') !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Titre', 'onkeyup' => 'stringToSlug(this.value)']) !!}
    </div>
</div>

<!-- slug Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('slug', 'Slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug', 'readonly']) !!}
    </div>
</div>

<!-- description Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('description', 'Description de l&rsquo;Article:') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'name' => 'description']) !!}
    </div>
</div>

<!-- publication_date Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('publication_date', 'Date de Publication:') !!}
        @if(isset($blog))
            {!! Form::date('publication_date', \Carbon\Carbon::parse($blog->publication_date)->format('Y-m-d') ?? null, ['class' => 'form-control', 'id' => 'start_of_work','value' => 0]) !!}
        @else
            {!! Form::date('publication_date', null, ['class' => 'form-control', 'id' => 'publication_date','value' => 0]) !!}
        @endif
    </div>
</div>
<!-- author Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('author', 'Auteur de l&rsquo;Article:') !!}
        {!! Form::text('author', null, ['class' => 'form-control', 'placeholder' => 'Auteur de l&rsquo;Article']) !!}
    </div>
</div>

<!-- category_blog_id Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('category_blog_id', 'Catégorie de l&rsquo;Article:') !!}
        <select name="category_blogs_id" class="form-select" id="basicSelect">
            @foreach($categoryBlogs as $categoryBlog)
                <option value="{{$categoryBlog->id}}">{{$categoryBlog->name}}</option>
            @endforeach
        </select>
    </div>
</div>

<!-- image Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('image', 'Image de garde:') !!}
        {!! Form::file('image', ['class' => 'form-control', 'id' => 'formFile']) !!}
    </div>
</div>

<!-- medias Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('medias', 'Images de l&rsquo;Article:') !!}
        {!! Form::file('medias[]', ['class' => 'form-control', 'id' => 'formFileMultiple', 'multiple' => true, 'accept' => 'image/*']) !!}
    </div>
</div>

<div class="col-12 d-flex justify-content-end mt-3">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-danger me-1 mb-1']) !!}
    <a href="{{ route('blogs.index') }}" class="btn btn-light-secondary me-1 mb-1">Annulé</a>
</div>

<script>
    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }

    function showPercentage(val){
        if(val == 1){
            document.getElementById("div_percentage").style.display= 'block';
        }else{
            document.getElementById("div_percentage").style.display= 'none';
        }
    }

    function showSold(val){
        if(val == 1){
            document.getElementById("div_sold").style.display= 'block';
        }else{
            document.getElementById("div_sold").style.display= 'none';
        }
    }
</script>
