<div class="row">
    <!-- Title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre de l&rsquo;Article:') !!}
            <p>{{ $blog->title }}</p>
        </div>
    </div>
    <!-- Slug At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('slug', 'slug:') !!}
            <p>{{ $blog->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Description At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('description', 'Description de l&rsquo;Article:') !!}
            <p>{!! $blog->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- publication_date At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('publication_date', 'Date de Publication:') !!}
            <p>{{ $blog->publication_date }}</p>
        </div>
    </div>
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            <p><img src="{{ asset('/storage/images/' . $blog->image) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- author At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('author', 'Auteur de l&rsquo;Article:') !!}
            <p>{{ $blog->author }}</p>
        </div>
    </div>
    <!-- category_blog_id At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('category_blog_id', 'Catégorie de l&rsquo;Article:') !!}
            <p>{{ $blog->getCategoryBlogsAttribute()->name ?? '---' }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Medias At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('medias', 'Autres images:') !!}
            <p>
                @if ($blog->file == null)
                    @php
                    $files = explode(',',trim(stripslashes($blog->medias),"[]"));
                    @endphp
                    @foreach ($files as $item)
                        @php
                        $item = trim($item, '"');
                        $url = "/storage/$item";
                        $arr = explode('/', $url);
                        $last = end($arr);
                        $download_link = "/download/$last";
                        @endphp
                        <a href="{{ url($download_link) }}">
                            <img src="{{ asset($url) }}" width="25%"/>
                        </a>
                    @endforeach
                @else
                    Aucune image de disponible
                @endif
            </p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $blog->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $blog->updated_at }}</p>
        </div>
    </div>
</div>
