@extends('BackOffice_layouts.master_index')

@section('title')
    Notre Equipe | {{ config('app.name') }}
@endsection

@section('content')

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color: white !important">Notre Equipe</h3>
                    <p class="text-subtitle text-muted" style="color: white !important">La liste de tous nos Employés</p>
                </div>
                <div class="col-12 col-md-6 order-md-2 order-first">
                    <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                        <ol class="breadcrumb">
                            <a class="btn btn-danger float-right"href="{{ route('teams.create') }}">Ajouter un Employé</a>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <section class="section">
            <div class="card">
                @include('teams.table')
            </div>
        </section>
    </div>

@endsection

