<!-- Title Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('name', 'Nom de l&rsquo;employé:') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nom de l&rsquo;employé']) !!}
    </div>
</div>

<!-- Title Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('post', 'Poste occupé:') !!}
        {!! Form::text('post', null, ['class' => 'form-control', 'placeholder' => 'Poste occupé']) !!}
    </div>
</div>
{{-- 
<!-- description Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('description', 'Message du membre:') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'name' => 'description']) !!}
    </div>
</div> --}}

<!-- image Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('image', 'Photo:') !!}
        {!! Form::file('image', ['class' => 'form-control', 'id' => 'formFile']) !!}
    </div>
</div>

<div class="col-12 d-flex justify-content-end mt-3">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-danger me-1 mb-1']) !!}
    <a href="{{ route('teams.index') }}" class="btn btn-light-secondary me-1 mb-1">Annulé</a>
</div>
