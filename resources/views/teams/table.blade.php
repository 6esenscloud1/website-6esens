<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Image</th>
                <th>Nom de l'Employé</th>
                <th>Post Occupé</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($teams as $team)
                <tr>

                    <td><img src="{{ asset('/storage/images/' .$team->image) }}" alt="{{ $team->name }}" width="90"></td>
                    <td>{{ $team->name }}</td>
                    <td>{{ $team->post }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['teams.destroy', $team->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('teams.show', [$team->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                <button type="button" class="btn btn-warning"><a href="{{ route('teams.edit', [$team->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
