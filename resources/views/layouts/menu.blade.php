<li class="nav-item">
    <a href="{{ route('cS.index') }}"
       class="nav-link {{ Request::is('cS*') ? 'active' : '' }}">
        <p>C S</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('slides.index') }}"
       class="nav-link {{ Request::is('slides*') ? 'active' : '' }}">
        <p>Slides</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('projects.index') }}"
       class="nav-link {{ Request::is('projects*') ? 'active' : '' }}">
        <p>Projects</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('categoryProjects.index') }}"
       class="nav-link {{ Request::is('categoryProjects*') ? 'active' : '' }}">
        <p>Category Projects</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('servicios.index') }}"
       class="nav-link {{ Request::is('servicios*') ? 'active' : '' }}">
        <p>Servicios</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('typeServices.index') }}"
       class="nav-link {{ Request::is('typeServices*') ? 'active' : '' }}">
        <p>Type Services</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('contactRequests.index') }}"
       class="nav-link {{ Request::is('contactRequests*') ? 'active' : '' }}">
        <p>Contact Requests</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('settings.index') }}"
       class="nav-link {{ Request::is('settings*') ? 'active' : '' }}">
        <p>Settings</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('newLetters.index') }}"
       class="nav-link {{ Request::is('newLetters*') ? 'active' : '' }}">
        <p>New Letters</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('roles.index') }}"
       class="nav-link {{ Request::is('roles*') ? 'active' : '' }}">
        <p>Roles</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('teams.index') }}"
       class="nav-link {{ Request::is('teams*') ? 'active' : '' }}">
        <p>Teams</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('blogs.index') }}"
       class="nav-link {{ Request::is('blogs*') ? 'active' : '' }}">
        <p>Blogs</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('categoryBlogs.index') }}"
       class="nav-link {{ Request::is('categoryBlogs*') ? 'active' : '' }}">
        <p>Category Blogs</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('users.index') }}"
       class="nav-link {{ Request::is('users*') ? 'active' : '' }}">
        <p>Users</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('roles.index') }}"
       class="nav-link {{ Request::is('roles*') ? 'active' : '' }}">
        <p>Roles</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('partners.index') }}"
       class="nav-link {{ Request::is('partners*') ? 'active' : '' }}">
        <p>Partners</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('candidacies.index') }}"
       class="nav-link {{ Request::is('candidacies*') ? 'active' : '' }}">
        <p>Candidacies</p>
    </a>
</li>


