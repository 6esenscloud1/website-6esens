<div class="row">
    <!-- Title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre du Service:') !!}
            <p>{{ $servicio->title }}</p>
        </div>
    </div>
    <!-- slug At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $servicio->slug }}</p>
        </div>
    </div>
</div>

{{-- <div class="row mt-3">
    <!-- Description At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('description', 'Description du Service:') !!}
            <p>{!! $servicio->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Subtitle At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('subtitle', 'Sous-Titre:') !!}
            <p>{{ $servicio->subtitle }}</p>
        </div>
    </div>
    <!-- type_service_id At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('type_service_id', 'Catégorie du Service:') !!}
            <p>{{ $servicio->getTypeServicesAttribute()->title ?? '---' }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Image At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            <p><img src="{{ asset('/storage/images/' . $servicio->image) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
</div> --}}

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $servicio->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $servicio->updated_at }}</p>
        </div>
    </div>
</div>
