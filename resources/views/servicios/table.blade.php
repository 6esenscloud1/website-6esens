<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>

                <th>Titre du Service</th>
                <th>Slug</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($servicios as $servicio)
                <tr>

                    <td>{{ $servicio->title }}</td>
                    <td>{{ $servicio->slug }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['servicios.destroy', $servicio->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('servicios.show', [$servicio->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                <button type="button" class="btn btn-warning"><a href="{{ route('servicios.edit', [$servicio->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
