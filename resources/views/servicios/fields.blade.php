<!-- id Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('id', 'Identifiant du Service:') !!}
        {!! Form::text('id', null, ['class' => 'form-control', 'placeholder' => 'Identifiant']) !!}
    </div>
</div>

<!-- Title Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('title', 'Titre du Service:') !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Titre', 'onkeyup' => 'stringToSlug(this.value)']) !!}
    </div>
</div>

<!-- slug Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('slug', 'Slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug', 'readonly']) !!}
    </div>
</div>

{{-- <!-- description Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('description', 'Description du Service:') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'name' => 'description']) !!}
    </div>
</div>

<!-- subtitle Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('subtitle', 'Sous-Titre du Service:') !!}
        {!! Form::text('subtitle', null, ['class' => 'form-control', 'placeholder' => 'Sous-Titre']) !!}
    </div>
</div>

<!-- type_service_id Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('type_service_id', 'Catégorie du Service:') !!}
        <select class="choices form-select multiple-remove" multiple="multiple">
            @foreach($typeServices as $typeService)
                <option value="{{$typeService->id}}">{{$typeService->title}}</option>
            @endforeach
        </select>
    </div>
</div>

<!-- image Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('image', 'Image de garde:') !!}
        {!! Form::file('image', ['class' => 'form-control', 'id' => 'formFile']) !!}
    </div>
</div> --}}

<div class="col-12 d-flex justify-content-end mt-3">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-danger me-1 mb-1']) !!}
    <a href="{{ route('servicios.index') }}" class="btn btn-light-secondary me-1 mb-1">Annulé</a>
</div>

<script>
    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }

    function showPercentage(val){
        if(val == 1){
            document.getElementById("div_percentage").style.display= 'block';
        }else{
            document.getElementById("div_percentage").style.display= 'none';
        }
    }

    function showSold(val){
        if(val == 1){
            document.getElementById("div_sold").style.display= 'block';
        }else{
            document.getElementById("div_sold").style.display= 'none';
        }
    }
</script>
