<div class="row">
    <!-- Title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre de la Campagne:') !!}
            <p>{{ $project->title }}</p>
        </div>
    </div>
    <!-- Slug At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('slug', 'slug:') !!}
            <p>{{ $project->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Project_owner At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('length_campaign', 'Durée de la Campagne:') !!}
            <p>{{ $project->length_campaign }}</p>
        </div>
    </div>
    <!-- customer_campaign At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('customer_campaign', 'Nom du Client:') !!}
            <p>{{ $project->customer_campaign }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Description At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('description', 'Description de la Réalisation:') !!}
            <p>{!! $project->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- support At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('support', 'Support de Communication:') !!}
            <p>{{ $project->support }}</p>
        </div>
    </div>
    <!-- categoryproject_id At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('categoryproject_id', 'Catégorie du Projet:') !!}
            <p>{{ $project->getCategoryProjectAttribute()->title ?? '---' }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- creative_director At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('creative_director', 'Directeur Créative:') !!}
            <p>{{ $project->creative_director }}</p>
        </div>
    </div>
    <!-- project_leader At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('project_leader', 'Chef de Projet:') !!}
            <p>{{ $project->project_leader }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    @if (!is_null($project->image))
        <!-- Image At Field -->
        <div class="col-md-6 col-12">
            <div class="form-group">
                {!! Form::label('image', 'Image de Garde:') !!}
                <p><img src="{{ asset('/storage/images/' . $project->image) }}" alt="" width="250" height="200"></p>
            </div>
        </div>
    @else
        <!-- link_video At Field -->
        <div class="col-md-12 col-12">
            <div class="form-group">
                {!! Form::label('link_video', 'Lien de la Vidéo:') !!}
                <p>{{ $project->link_video }}</p>
                <iframe class="embed-responsive-item" src="{{ $project->link_video }}"></iframe>
            </div>
        </div>

    @endif


</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $project->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $project->updated_at }}</p>
        </div>
    </div>
</div>






