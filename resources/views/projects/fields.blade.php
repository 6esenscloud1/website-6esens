<!-- Title Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('title', 'Titre de la Campagne:') !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Titre', 'onkeyup' => 'stringToSlug(this.value)']) !!}
    </div>
</div>

<!-- slug Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('slug', 'Slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug', 'readonly']) !!}
    </div>
</div>

<!-- project_owner Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('length_campaign', 'Durée de la Campagne:') !!}
        {!! Form::text('length_campaign', null, ['class' => 'form-control', 'placeholder' => 'Durée de la Campagne']) !!}
    </div>
    <span style="font-weight: 900">Exemple : 2 mois</span>
</div>

<!-- attendees Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('customer_campaign', 'Nom du Client:') !!}
        {!! Form::text('customer_campaign', null, ['class' => 'form-control', 'placeholder' => 'Nom du Client']) !!}
    </div>
</div>

<!-- description Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('description', 'Description de la Campagne:') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'name' => 'description']) !!}
    </div>
</div>

<!-- support Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('support', 'Support de Communication:') !!}
        {!! Form::text('support', null, ['class' => 'form-control', 'placeholder' => 'Support de Communication']) !!}
    </div>
    <span style="font-weight: 900">Exemple : PRINT - DIGITAL - TV</span>
</div>

<!-- cateorie_projet_id Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('category_project_id', 'Catégorie de Réalisation:') !!}
        <select class="form-select" id="category_project_id" name="category_project_id">
            <option value="">Aucune Catégorie</option>
            @foreach($categoryProjects as $categoryProject)
                <option value="{{$categoryProject->id}}">{{$categoryProject->title}}</option>
            @endforeach
        </select>
    </div>
</div>

<!-- project_leader Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('project_leader', 'Chef de Projet:') !!}
        {!! Form::text('project_leader', null, ['class' => 'form-control', 'placeholder' => 'Chef de Projet']) !!}
    </div>
</div>

<!-- creative_director Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('creative_director', 'Directeur Créative:') !!}
        {!! Form::text('creative_director', null, ['class' => 'form-control', 'placeholder' => 'Directeur Créative']) !!}
    </div>
</div>


<!-- image Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('image', 'Image de garde:') !!}
        {!! Form::file('image', ['class' => 'form-control', 'id' => 'formFile']) !!}
    </div>
</div>

<!-- link_video Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('link_video', 'Lien de la Vidéo:') !!}
        {!! Form::text('link_video', null, ['class' => 'form-control', 'placeholder' => 'Lien de la vidéo']) !!}
    </div>
    <small>Exemple: pour le lien suivant https://www.youtube.com/watch?v=mmq5zZfmIws vous devrez enregistrer l'id de la video qui est <strong>"mmq5zZfmIws"</strong></small>
</div>


<div class="col-12 d-flex justify-content-end mt-3">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-danger me-1 mb-1']) !!}
    <a href="{{ route('projects.index') }}" class="btn btn-light-secondary me-1 mb-1">Annulé</a>
</div>

<script>
    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }

    function showPercentage(val){
        if(val == 1){
            document.getElementById("div_percentage").style.display= 'block';
        }else{
            document.getElementById("div_percentage").style.display= 'none';
        }
    }

    function showSold(val){
        if(val == 1){
            document.getElementById("div_sold").style.display= 'block';
        }else{
            document.getElementById("div_sold").style.display= 'none';
        }
    }
</script>
