@extends('Customer_FrontOffice.layoutsFrontoffice.masterPage')

@section('title')
    A Propos | {{ config('app.name') }}
@endsection

@section('content')

    <div class="header-p">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark menu-header">

            <!--  Show this only on mobile to medium screens  -->
            <a class="navbar-brand d-lg-none" href="{{url ('/') }}">
                <img src="{{asset('customerFront/images/Home/LOGO-6SENS-BLANC.png')}}" alt="" srcset="" class="home-logo">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle"
                aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!--  Use flexbox utility classes to change how the child elements are justified  -->
            <div class="collapse navbar-collapse justify-content-between" id="navbarToggle">

                <ul class="navbar-nav item-menu">
                    <li class="nav-item">
                        <a class="nav-link active" href="{{route ('about') }}">A PROPOS <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('services') }}">SERVICES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">NOUS REJOINDRE</a>
                    </li>
                </ul>


                <!--   Show this only lg screens and up   -->
                <a class="navbar-brand d-none d-lg-block" href="{{url ('/') }}">
                    <img src="{{asset('customerFront/images/Home/LOGO-6SENS-BLANC.png')}}" alt="" srcset="" class="home-logo">
                </a>
                <ul class="navbar-nav btn-nr">
                    <li class="nav-item">
                        <a class="nav-link btn-rejoindre" href="{{route ('contact') }}">COLLABORONS</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="img-bg content-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6"></div>
                    <div class="col-lg-6">
                        <div class="bg-img">
                            <img src="{{asset('customerFront/images/Apropos/bg-apropos.png')}}" alt="" srcset="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="section-apropos content-section">
            <div class="container">
                <div class="simple-tt">
                    <h5>A PROPOS DE NOTRE AGENCE</h5>
                </div>
                <div class="title">
                    <h1>
                        nous VOUS Aidons À <br />
                        CONSTRUIRE DES CAMPAGNES<br />
                        UNIQUES SUR LA SCÈNE AFRICAINE.
                    </h1>
                </div>
                <div class="content-apropos">
                    <p>
                        Implantée à Abidjan en Côte d’Ivoire depuis 2010, l’Agence travaille aujourd’hui sur 11 pays en
                        Afrique de l’Ouest et centrale.
                        Notre réseau de partenaires a permis de satisfaire nos clients et mener à bien des campagnes en Côte
                        d’Ivoire principalement
                        mais aussi en Guinée, au Mali, au Burkina Faso, au Togo, au Bénin, au Cameroun, au Gabon et aux 2
                        Congo.<br /><br />

                        Depuis 2016, une seconde agence est implantée à Niamey,
                        au Niger pour répondre aux besoins spécifiques des pays de la zone sahélienne.

                    </p>

                    <!-- Button trigger modal -->
                    <div class="modal--">
                        <button type="button" class="btn-network" data-toggle="modal" data-target="#exampleModal">
                            DÉCOUVRIR <br />NOTRE RÉSEAU D'AGENCE
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        ...
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-equipe content-section">
            <div class="container">
                <h1 class="title- py-5">NOTRE ÉQUIPE</h1>
                <div class="row">
                    @foreach ($teams as $team)
                        <div class="col-md-4 col-sm-6 mt-3">
                            <div class="box">
                                <img src="{{ asset('/storage/images/' . $team->image) }}"/>
                                <div class="box-content">
                                    <h3 class="title">{{$team->name}}</h3>
                                    <span class="post">{{$team->post}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>

@endsection
