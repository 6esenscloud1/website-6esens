@extends('Customer_FrontOffice.layoutsFrontoffice.masterPage')

@section('title')
    Nos Services | {{ config('app.name') }}
@endsection

@section('content')

    <div class="header-p header">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark menu-header">

            <!--  Show this only on mobile to medium screens  -->
            <a class="navbar-brand d-lg-none" href="{{url ('/') }}">
                <img src="{{asset('customerFront/images/Home/LOGO-6SENS-BLANC.png')}}" alt="" srcset="" class="home-logo">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle"
                aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!--  Use flexbox utility classes to change how the child elements are justified  -->
            <div class="collapse navbar-collapse justify-content-between" id="navbarToggle">

                <ul class="navbar-nav item-menu">
                    <li class="nav-item">
                        <a class="nav-link active" href="{{route ('about') }}">A PROPOS <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('services') }}">SERVICES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">NOUS REJOINDRE</a>
                    </li>
                </ul>


                <!--   Show this only lg screens and up   -->
                <a class="navbar-brand d-none d-lg-block" href="{{url ('/') }}">
                    <img src="{{asset('customerFront/images/Home/LOGO-6SENS-BLANC.png')}}" alt="" srcset="" class="home-logo">
                </a>
                <ul class="navbar-nav btn-nr">
                    <li class="nav-item">
                        <a class="nav-link btn-rejoindre" href="{{route ('contact') }}">COLLABORONS</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="img-bg content-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6"></div>
                    <div class="col-lg-6">
                        <div class="bg-img">
                            <img src="{{asset('customerFront/images/Apropos/bg-apropos.png')}}" alt="" srcset="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="section-apropos content-section">
            <div class="container">
                <div class="simple-tt">
                    <h5>NOTRE ÉVENTAIL DE SERVICES</h5>
                </div>
                <div class="title">
                    <h1>
                        NOUS ÉLABORONS DES <br />
                        STRATÉGIES EN PRISE AVEC LES <br />
                        RÉALITÉS LOCALES.
                    </h1>
                </div>
                <div class="content-apropos">
                    <p>
                        Grâce à notre connaissance approfondie de diverses l'industries <br />
                        à savoir l’agroalimentaire, la finance, les NTIC, les Télécom, nous <br />
                        arrivons à proposer à nos clients diverses services qui comprennent <br />
                        des campagnes intégrées, la planification et l'achat de médias, la <br />
                        conception stratégique, la publicité, les relations publiques, le marketing digital.

                    </p>
                </div>
            </div>
        </div>
        <div class="section-services content-section">
            <div class="container">
                <div class="row concept-creatif line">
                    <div class="col-sm-6">
                        <div class="img-concept-creatif" data-aos="fade-up">
                            <img src="{{asset('customerFront/images/Services/concept-1.png')}}" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="content-concept-creatif" data-aos="fade-left">
                            <h1 class="title-">CONCEPT CRÉATIF</h1>
                            <p>
                                Agence conseil en communication à 360°, Sixième
                                Sens propose une expertise marketing exhaustive.
                                Nos créatifs et stratèges mettent en synergie leurs
                                compétences pour concevoir et mettre en place des dispositifs stratégiques
                                de campagnes pertinentes et différenciantes
                            </p>
                            <div class="second-img--">
                                <img src="{{asset('customerFront/images/Services/concept-2.png')}}" alt="" srcset="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row activation line" data-aos="zoom-in-left">
                    <div class="col-sm-6">
                        <div class="content-activation">
                            <h1 class="title-">ACTIVATION BTL</h1>
                            <p>
                                Pour être efficace, tout plan de communication
                                terrain doit se révéler différenciant et modulable.
                                L'agence travail au quotidien à trouver des solutions créatives
                                innovantes, afin de créer un lien direct entre consommateurs et marques.
                                Nos opérations BTL sont en contact permanent avec les marchés.
                                Nous les adaptons créativement à votre stratégie globale,
                                nous suivons les résultats en continu et, au besoin,
                                aménageons le dispositif pour le rendre plus adéquat.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="img-activation">
                            <img src="{{asset('customerFront/images/Services/activation.png')}}" alt="" srcset="">
                        </div>
                    </div>
                </div>
                <div class="row media-p line" data-aos="zoom-in-right">
                    <div class="col-sm-6">
                        <div class="img-media-p">
                            <img src="{{asset('customerFront/images/Services/media-p.png')}}" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="content-media-p">
                            <h1 class="title-">MÉDIA PLANNING</h1>
                            <p>
                                Communication des plus interactives, le digital tient une place dans
                                les stratégies marketing efficaces. Notre marché en pleine croissance,
                                est un territoire à fort potentiel pour les nouvelles stratégies digitales.
                                Nos équipes créatives et stratégiques les intègrent à leurs processus,
                                en les adaptant aux usages locaux des NTICS. Booster et relais de nos lancements
                                traditionnels, ou opérations spécifiques, le marketing digital permet une
                                communication peut coûteuse, personnalisée, rapide, interactive et donc efficiente.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row relation-p line" data-aos="fade-up" data-aos-anchor-placement="top-center">
                    <div class="col-sm-2">
                        <div class="img-relation">
                            <img src="{{asset('customerFront/images/Services/RP.png')}}" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="content-relation">
                            <h1 class="title-">RELATION PUBLIQUES</h1>
                            <p>
                                Nos équipes conçoivent des stratégies média pertinentes,
                                ajustées et qualitatives, ainsi que toute la gamme des
                                offres médias traditionnelles, pour assurer une présence
                                visible et tactique à vos marques. Forte d'une expérience
                                du marché dans lequel elle est implantée, l'Agence fait
                                pour vous la différence pour réaliser vos objectifs qu'elle
                                fait siens. Ancrée dans le réel des cultures locales et
                                adepte d'une méthodologie transparente basée sur des
                                données claires, elle permettra à tous instant de
                                maximiser l'impact de vos actions média.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="img-relation">
                            <img src="{{asset('customerFront/images/Services/RP.png')}}" alt="" srcset="">
                        </div>
                    </div>
                </div>
                <div class="tt-comm">
                    <h1 class="title-">COMMUNICATION DIGITALE</h1>
                </div>
                <div class="row communication" data-aos="flip-right">
                    <div class="col-sm-6">
                        <div class="img-digital">
                            <img src="{{asset('customerFront/images/Services/comm-digitale.png')}}" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="content-comm-digital">
                            <p>
                                Communication des plus interactives, le digital tient une place dans les stratégies
                                marketing efficaces.
                                Notre marché en pleine croissance, est un territoire à fort potentiel pour les
                                nouvelles stratégies digitales. Nos équipes créatives et stratégiques les intègrent
                                à leurs processus, en les adaptant aux usages locaux des NTICS. Booster et relais de nos
                                lancements
                                traditionnels, ou opérations spécifiques, le marketing digital permet une communication peut
                                coûteuse,
                                personnalisée, rapide, interactive et donc efficiente.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slide-devis">
        <div class="scroll-">
            <span class="txt t1"><a href="{{route ('contact') }}">DEMANDEZ UN DEVIS&nbsp;</a></span>
            <span class="txt t2"><a href="{{route ('contact') }}">DEMANDEZ UN DEVIS&nbsp;</a></span>
        </div>
    </div>

@endsection
