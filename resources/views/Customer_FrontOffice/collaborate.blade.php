@extends('Customer_FrontOffice.layoutsFrontoffice.masterContact')

@section('title')
    Collaborons | {{ config('app.name') }}
@endsection

@section('content')

    <div class="header-c">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark menu-header">

            <!--  Show this only on mobile to medium screens  -->
            <a class="navbar-brand d-lg-none" href="{{url ('/') }}">
                <img src="{{asset('customerFront/images/Home/LOGO-6SENS-BLANC.png')}}" alt="" srcset="" class="home-logo">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle"
                aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!--  Use flexbox utility classes to change how the child elements are justified  -->
            <div class="collapse navbar-collapse justify-content-between" id="navbarToggle">

                <ul class="navbar-nav item-menu">
                    <li class="nav-item">
                        <a class="nav-link active" href="{{route ('about') }}">A PROPOS <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route ('services') }}">SERVICES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">NOUS REJOINDRE</a>
                    </li>
                </ul>


                <!--   Show this only lg screens and up   -->
                <a class="navbar-brand d-none d-lg-block" href="{{url ('/') }}">
                    <img src="{{asset('customerFront/images/Home/LOGO-6SENS-BLANC.png')}}" alt="" srcset="" class="home-logo">
                </a>
                <ul class="navbar-nav btn-nr">
                    <li class="nav-item">
                        <a class="nav-link btn-rejoindre" href="{{route ('contact') }}">COLLABORONS</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="i-bg content-section">
            <div class="i--g">
                <img src="{{asset('customerFront/images/Contact/contact.png')}}" alt="" srcset="">
            </div>
        </div>
    </div>
    <div class="content">
        <div class="section-apropos content-section new-sec">
            <div class="container">
                <div class="simple-tt">
                    <h5>FAITES NOUS PART DE VOTRE BESOIN</h5>
                </div>
                <div class="title">
                    <h1>DONNONS VIE A VOTRE PROJET ?</h1>
                </div>
            </div>
        </div>
        <div class="section-content-c">
            <form action="{{ url('contacter') }}" method="POST" id="contact-form">
                @csrf
                <div class="search">
                    <div class="container">
                        <div class="title-choice">
                            <h1>QUE RECHERCHEZ-VOUS ?</h1>
                        </div>
                        <div class="row">
                            @foreach ($typeServices as $typeService)
                                <div class="col-sm-6">
                                    <div class="sec-first sec-upd">
                                        <div class="img-first">
                                            <img src="{{ asset('/storage/images/' . $typeService->image) }}" alt="{{$typeService->title}}" srcset="">
                                        </div>
                                        <div class="sec">
                                            <ul>
                                                <li>
                                                    <input type="radio" id="{{$typeService->id}}">
                                                    <label for="{{$typeService->id}}">{{$typeService->title}}</label>

                                                    <div class="check"></div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="choice-service">
                    <div class="container">
                        <div class="title-choice">
                            <h1>DONNEZ-NOUS PLUS DE DÉTAILS</h1>
                            <p>
                                Choississez le service pour lequel
                                vous souhaitez être accompagné dans
                                la liste ci dessous
                            </p>
                        </div>
                        <div class="row">
                            @foreach ($servicios as $servicio)
                                <div class="col-sm-4">
                                    <div class="sec">
                                        <ul>
                                            <li>
                                                <input type="checkbox" id="{{$servicio->id}}">
                                                <label for="{{$servicio->id}}">{{$servicio->title}}</label>

                                                <div class="check"></div>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="for-contact">
                    <div class="title-choice">
                        <h1>LAISSEZ NOUS VOS COORDONNÉES</h1>
                        <p>
                            Notre service commercial se chargera de vous
                            contacter dans un délai maximal de 48 heures
                        </p>
                        <div class="container">
                            <div class="formBox">

                                <div class="inputBox ">
                                    <input type="text" required name="name" id="name" class="input @error('name') is-invalid @enderror" placeholder="Nom et Prénoms" value="{{old('name')}}">
                                </div>
                                <div class="inputBox ">
                                    <input type="text" required name="company" id="company" class="input @error('company') is-invalid @enderror" onclick="" placeholder="Entreprise / Organisation" value="{{old('company')}}">
                                </div>
                                <div class="inputBox ">
                                    <input type="text" required name="email" id="email" class="input @error('email') is-invalid @enderror" placeholder="Email" value="{{old('email')}}">
                                </div>
                                <div class="inputBox ">
                                    <input type="text" required name="phone" id="phone" class="input @error('phone') is-invalid @enderror" placeholder="Téléphone" value="{{old('phone')}}">
                                </div>
                                <div class="inputBox ">
                                    <input type="text" required name="position_held" id="position_held" class="input @error('position_held') is-invalid @enderror" placeholder="Poste occupé" value="{{old('position_held')}}">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 py-5">
                                        <button onclick="" type="submit" class="button" id="btn" value="Envoyer">Envoyer</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <script>

        $(document).ready(function(){

            $("#contact-form").submit(function(e){

                e.preventDefault();
                let url = $(this).attr('action');
                $("#btn").attr('disabled', true);

                $.post(url,
                {
                    '_token': $("#token").val(),
                    // type_services_id: $("#type_services_id").val(),
                    // servicio_id: $("#servicio_id").val(),
                    name: $("#name").val(),
                    company: $("#company").val(),
                    email: $("#email").val(),
                    phone: $("#phone").val(),
                    position_held: $("#position_held").val()
                },

                function (response) {

                    if(response.code == 400){
                        $("#btn").attr('disabled', false);
                        let error = '<span class="alert alert-danger">'+response.msg+'</span>';
                        $("#res").html(error);
                    }
                    else if(response.code == 200){
                        $("#btn").attr('disabled', false);
                        let success = '<span class="alert alert-success">'+response.msg+'</span>';
                        $("#res").html(success);
                    }

                });

            })
        })

    </script>

@endsection
