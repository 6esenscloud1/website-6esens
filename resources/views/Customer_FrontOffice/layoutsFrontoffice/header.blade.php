<div class="header">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark menu-header">

        <!--  Show this only on mobile to medium screens  -->
        <a class="navbar-brand d-lg-none" href="{{url ('/') }}">
            <img src="{{asset('customerFront/images/Home/LOGO-6SENS-BLANC.png')}}" alt="" srcset="" class="home-logo">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle" aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!--  Use flexbox utility classes to change how the child elements are justified  -->
        <div class="collapse navbar-collapse justify-content-between" id="navbarToggle">

            <ul class="navbar-nav item-menu">
                <li class="nav-item">
                    <a class="nav-link active" href="{{route ('about') }}">A PROPOS <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route ('services') }}">SERVICES</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">NOUS RÉJOINDRE</a>
                </li>
            </ul>


            <!--   Show this only lg screens and up   -->
            <a class="navbar-brand d-none d-lg-block" href="{{url ('/') }}">
                <img src="{{asset('customerFront/images/Home/LOGO-6SENS-BLANC.png')}}" alt="" srcset="" class="home-logo">
            </a>
            <ul class="navbar-nav btn-nr">
                <li class="nav-item">
                    <a class="nav-link btn-rejoindre" href="{{route ('contact') }}">COLLABORONS</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="video-bg content-section">
        <video autoplay muted loop id="myVideo">
            <source src="{{asset('customerFront/videos/Seoul_21118.mp4')}}" type="video/mp4">
        </video>
    </div>
</div>
