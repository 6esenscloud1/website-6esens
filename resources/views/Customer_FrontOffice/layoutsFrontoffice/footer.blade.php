<div class="footer-">
    <div class="container">
        <div class="row sec-footer-1">
            <div class="col-sm-6">
                <div class="project">
                    <h6>On discute projet ?</h6>
                    <a href="#">ÉCRIVEZ-NOUS ?</a>
                </div>
            </div>
            <div class="col-sm-6">
                <h6>Envie de nous rejoindre ?</h6>
                <a href="#">POSTULEZ ICI !</a>
            </div>
        </div>
        <div class="t-">
            <p>CONTACTER L'AGENCE</p>
        </div>
        <div class="row sec-footer-2">
            <div class="col-sm-4">
                <div class="logo">
                    <img src="{{asset('customerFront/images/Home/LOGO-6SENS-BLANC.png')}}" alt="" srcset="">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="link-to">
                    <ul>
                        <li><a href="{{route ('about') }}">A PROPOS</a></li>
                        <li><a href="{{route ('services') }}">SERVICES</a></li>
                        <li><a href="{{route ('contact') }}">COLLABORONS</a></li>
                        <li><a href="#">NOUS REJOINDRE</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="map">
                    <ul>
                        <li>Bvd VGE immeuble Massai
                            30 BP 382 Abidjan 30</li>
                        <li>(+225) 27 21 26 52 40</li>
                        <li>contact@agence6sens.fr</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row sec-footer-3">
        <div class="col-sm-6">
            <div class="rs-footer">
                <ul>
                    <li><a><i class="fa fa-facebook" aria-hidden="true">&nbsp;</i></a></li>
                    <li><a><i class="fa fa-youtube-play" aria-hidden="true">&nbsp;</i></a></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="mentions">
                <p>Mentions légales - Politique de confidentialité</p>
            </div>
        </div>
    </div>
</div>
