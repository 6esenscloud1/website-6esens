@extends('Customer_FrontOffice.layoutsFrontoffice.master')

@section('title')
    Accueil | {{ config('app.name') }}
@endsection

@section('content')

    <div class="content">
        <div class="section-agency content-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="txt-agency">
                            <h1 class="title-">NOTRE AGENCE</h1>
                            <p>
                                Nous sommes une agence conseil en communication à 360° basée à Abidjan depuis plus de 10 ans
                                avec une expertise en communication, une portée locale et internationale, des résultats
                                éprouvés et une énergie
                                inlassable pour porter loin votre entreprise.<br />
                                <br />
                                Avec notre équipe composée de créatifs et stratèges nous apportons à nos clients une
                                réflexion nouvelle , une expertise stratégique et une puissance créative pour des campagnes
                                authentiques et révolutionnaires.
                            </p>
                            <button type="submit" class="btn-black">EN SAVOIR PLUS</button>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="img-agency">
                            <img src="{{ asset('customerFront/images/Home/1.png') }}" alt="" srcset="" />
                        </div>
                        <div class="row sous-img">
                            <div class="col-sm-6">
                                <div class="S-img1">
                                    <img src="{{ asset('customerFront/images/Home/2.png') }}" alt="" srcset="" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="S-img2">
                                    <img src="{{ asset('customerFront/images/Home/2.png') }}" alt="" srcset="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-metho content-section">
            <div class="container">
                <div class="tabs">
                    <div class="tab">
                        <input type="checkbox" id="chck1" />
                        <label class="tab-label" for="chck1">01. PENSER STATÉGIE LOCALE</label>
                        <div class="tab-content">
                            Des talents cosmopolites, une méthodologie de travail à l’européenne et une maîtrise des marchés
                            locaux, c’est le cocktail explosif qui rend notre Agence en mesure de vous offrir une stratégie
                            forte en prise avec les
                            réalités du marché local.
                        </div>
                    </div>
                    <div class="tab">
                        <input type="checkbox" id="chck2" />
                        <label class="tab-label" for="chck2">02. RAISONNER - CHALLENGER</label>
                        <div class="tab-content">
                            Optimiser votre budget, faire plus avec moins, tel est notre défi quotidien. Notre éthique de
                            travail nous fait aller au-delà des objectifs fixés. A la clef, une communication taillée sur
                            mesure et percutante.
                        </div>
                    </div>
                    <div class="tab">
                        <input type="checkbox" id="chck3" />
                        <label class="tab-label" for="chck3">03. CRÉER LE DÉCALLAGE</label>
                        <div class="tab-content">
                            Un creuset de créatifs, rompus aux exigences et normes internationales, réunies pour vous
                            concocter des campagnes qui détonnent, créent le décalage nécessaire pour assurer à nos marques
                            le meilleur impact.
                        </div>
                    </div>
                    <div class="tab">
                        <input type="checkbox" id="chck4" />
                        <label class="tab-label" for="chck4">04. MAÎTRISER L'EXÉCUTION</label>
                        <div class="tab-content">
                            Une couverture à 360 °, une exigence dans le suivi et l’exécution des opérations déployées dans
                            le cadre des stratégies définies conjointement. Nos équipes contrôlent la qualité de l’exécution
                            de chaque action posée.
                            Telle est la garantie 6eme sens.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-realisation content-section">
        <div class="container">
            <h1 class="title-">NOS RÉALISATIONS</h1>
            <div class="project-sec">
                <div class="wrapper">
                    <ul class="tabs clearfix" data-tabgroup="first-tab-group">
                        @foreach ($categoryProjects as $categoryProject)
                            <li>
                                <a href="#tab1{{ $categoryProject->id }}" class="{{ $categoryProject->id == 1 ? 'active' : '' }}">{{ $categoryProject->title }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <section id="first-tab-group" class="tabgroup">
                        @foreach ($categoryProjects as $categoryProject)
                            <div id="tab1{{ $categoryProject->id }}"
                                class="{{ $categoryProject->id == 1 ? 'active' : '' }}">
                                <div class="owl-carousel owl-theme">
                                    @foreach ($categoryProject->projects as $project)
                                        <div class="item">
                                            <div class="bloc-realisations">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="img-realisation">
                                                            @if (!is_null($project->image))
                                                                <img src="{{ asset('/storage/images/' . $project->image) }}"
                                                                    alt="" srcset="" />
                                                            @else
                                                                <iframe class="embed-responsive-item"
                                                                    src="{{ $project->link_video }}" height="100%" width="100%"
                                                                    title="YouTube video player" frameborder="0"
                                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                                    allowfullscreen></iframe>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="content-realisation">
                                                            <h4 class="title-campagne">{{ $project->title }}</h4>
                                                            <ul>
                                                                <li>Client : &nbsp;&nbsp;<span class="style-content">{{ $project->customer_campaign }}</span></li>

                                                                <li>Durée de la compagne :&nbsp;&nbsp;<span class="style-content">{{ $project->length_campaign }}</span></li>

                                                                <li>Support : &nbsp;&nbsp;<span class="style-content">{{ $project->support }}</span></li>

                                                                <li>Direction créative : &nbsp;&nbsp;<span class="style-content">{{ $project->creative_director }}</span></li>

                                                                <li>Chef de projet : &nbsp;&nbsp;<span class="style-content">{{ $project->project_leader }}</span></li>

                                                            </ul>
                                                            <p class="description">
                                                                {!! $project->description !!}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </section>
                </div>
            </div>
        </div>
    </div>
    <div class="partenaire-section">
        <div class="txt--partenaire">
            <h1>ILS NOUS ONT FAIT <br/> CONFIANCE</h1>
        </div>
        <div id="container-partenaire">
            <div class="photobanner">
                <img src="{{asset('customerFront/images/img_scroll1.png')}}" alt="" srcset="">
                <img src="{{asset('customerFront/images/img_scroll1.png')}}" alt="" srcset="">
            </div>
        </div>
        <div id="container-partenaire">
            <div class="photobanner">
                <img src="{{asset('customerFront/images/img_scroll2.png')}}" alt="" srcset="">
                <img src="{{asset('customerFront/images/img_scroll2.png')}}" alt="" srcset="">
            </div>
        </div>
    </div> 

</div>


@endsection
