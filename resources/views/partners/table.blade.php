<div class="table-responsive">
    <table class="table" id="partners-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Links</th>
        <th>Logo</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($partners as $partners)
            <tr>
                <td>{{ $partners->name }}</td>
            <td>{{ $partners->links }}</td>
            <td>{{ $partners->logo }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['partners.destroy', $partners->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('partners.show', [$partners->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('partners.edit', [$partners->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
