<div class="row">
    <!-- name At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('name', 'Titre de la Catégorie:') !!}
            <p>{{ $categoryBlog->name }}</p>
        </div>
    </div>
    <!-- Slug At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('slug', 'slug:') !!}
            <p>{{ $categoryBlog->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $categoryBlog->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $categoryBlog->updated_at }}</p>
        </div>
    </div>
</div>
