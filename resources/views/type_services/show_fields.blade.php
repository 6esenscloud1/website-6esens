<div class="row mt-3">
    <!-- title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre :') !!}
            <p>{{ $typeServices->title }}</p>
        </div>
    </div>
    <!-- slug At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $typeServices->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Image At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            <p><img src="{{ asset('/storage/images/' . $typeServices->image) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
</div>

<div class="row">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        {!! Form::label('created_at', 'Crée à:') !!}
        <p>{{ $typeServices->created_at }}</p>
    </div>

    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        {!! Form::label('updated_at', 'Modifier à:') !!}
        <p>{{ $typeServices->updated_at }}</p>
    </div>
</div>


