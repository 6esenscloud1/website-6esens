<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Image Descriptif</th>
                <th>Titre du Type d'Activation</th>
                <th>Slug</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($typeServices as $typeServices)
                <tr>

                    <td><img src="{{ asset('/storage/images/' .$typeServices->image) }}" alt="{{ $typeServices->title }}" width="90"></td>
                    <td>{{ $typeServices->title }}</td>
                    <td>{{ $typeServices->slug }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['typeServices.destroy', $typeServices->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('typeServices.show', [$typeServices->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                <button type="button" class="btn btn-warning"><a href="{{ route('typeServices.edit', [$typeServices->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
