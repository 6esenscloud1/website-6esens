@extends('BackOffice_layouts.master_index')

@section('title')
    Nos Types d'Activation | {{ config('app.name') }}
@endsection

@section('content')

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color: white !important">Nos Types d'Activation</h3>
                    <p class="text-subtitle text-muted" style="color: white !important">La liste de tous nos Types d'Activation</p>
                </div>
                <div class="col-12 col-md-6 order-md-2 order-first">
                    <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                        <ol class="breadcrumb">
                            <a class="btn btn-danger float-right"href="{{ route('typeServices.create') }}">Ajouter un Type Activation</a>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <section class="section">
            <div class="card">
                @include('type_services.table')
            </div>
        </section>
    </div>

@endsection

