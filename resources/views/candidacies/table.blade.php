<div class="card-body">

    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Noms & Prénoms</th>
                <th>Email</th>
                <th>Téléphone</th>
                <th>Poste Souhaité</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($candidacies as $candidacy)
                <tr>

                    <td>{{ $candidacy->name }}</td>
                    <td>{{ $candidacy->email }}</td>
                    <td>{{ $candidacy->phone }}</td>
                    <td>{{ $candidacy->desired_position }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['candidacies.destroy', $candidacy->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('candidacies.show', [$candidacy->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
