@extends('BackOffice_layouts.master_index')

@section('title')
    Candidature Spontanée | {{ config('app.name') }}
@endsection

@section('content')

     <div class="page-heading">
         <div class="page-title">
             <div class="row">
                 <div class="col-12 col-md-6 order-md-1 order-last">
                     <h3 style="color: white !important">Candidature Spontanée</h3>
                     <p class="text-subtitle text-muted" style="color: white !important">La liste de toutes nos Candidatures Spontanée</p>
                 </div>
                 <div class="col-12 col-md-6 order-md-2 order-first">
                     {{-- <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                         <ol class="breadcrumb">
                             <a class="btn btn-danger float-right"href="{{ route('categoryBlogs.create') }}">Ajouter une Catégorie</a>
                         </ol>
                     </nav> --}}
                 </div>
             </div>
         </div>
         <section class="section">
             <div class="card">
                 @include('candidacies.table')
             </div>
         </section>
     </div>

 @endsection
