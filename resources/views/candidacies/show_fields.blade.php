<div class="row">
    <!-- Name Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('name', 'Nom & Prénoms:') !!}
            <p>{{ $candidacy->name }}</p>
        </div>
    </div>
    <!-- Email Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <p>{{ $candidacy->email }}</p>
        </div>
    </div>
    <!-- Phone Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('phone', 'Téléphone:') !!}
            <p>{{ $candidacy->phone }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Birth Date Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('birth_date', 'Date de Naissance:') !!}
            <p>{{ $candidacy->birth_date }}</p>
        </div>
    </div>
    <!-- Birthplace Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('birthplace', 'Lieu de Naissance:') !!}
            <p>{{ $candidacy->birthplace }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Desired Position Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('desired_position', 'Poste Souhaité:') !!}
            <p>{{ $candidacy->desired_position }}</p>
        </div>
    </div>
    <!-- Curriculum Vitae Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('curriculum_vitae', 'Curriculum Vitae:') !!}
            <p>{{ $candidacy->curriculum_vitae }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('motivation_letter', 'Lettre de Motivation') !!}
            <p>{{ $candidacy->motivation_letter }}</p>
        </div>
    </div>
    <div class="col-md-6 col-12">
        <div class="form-group">

        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{{ $candidacy->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Updated At:') !!}
            <p>{{ $candidacy->updated_at }}</p>
        </div>
    </div>
</div>




