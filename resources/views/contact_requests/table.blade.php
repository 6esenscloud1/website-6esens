<div class="card-body">

    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Noms & Prénoms</th>
                <th>Email</th>
                <th>Téléphone</th>
                <th>Entreprise</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($contactRequests as $contactRequest)
                <tr>

                    <td>{{ $contactRequest->name }}</td>
                    <td>{{ $contactRequest->email }}</td>
                    <td>{{ $contactRequest->phone }}</td>
                    <td>{{ $contactRequest->company }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['contactRequests.destroy', $contactRequest->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('contactRequests.show', [$contactRequest->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
