<div class="row">
    <!-- Name At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('name', 'Nom du Client:') !!}
            <p>{{ $contactRequest->name }}</p>
        </div>
    </div>
    <!-- Phone At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('phone', 'Contact du Client:') !!}
            <p>{{ $contactRequest->phone }}</p>
        </div>
    </div>
</div>

<div class="row">
    <!-- type_service_id At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('type_service_id', 'Type d&rsquo;Activation Choisir:') !!}
            <p>{!! $contactRequest->getTypeServiceAttribute()->title ?? '---' !!}</p>
        </div>
    </div>
</div>

<div class="row">
    <!-- Subject At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('servicio_id', 'Service Souhaité:') !!}
            <p>{{ $contactRequest->servicio_id }}</p>
        </div>
    </div>
    <!-- Email At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('email', 'Email du Client:') !!}
            <p>{{ $contactRequest->email }}</p>
        </div>
    </div>
</div>

<div class="row">
    <!-- Email At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('company', 'Nom de l&rsquo;Entreprise:') !!}
            <p>{{ $contactRequest->company }}</p>
        </div>
    </div>
    <!-- Subject At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('position_held', 'Poste Occupé:') !!}
            <p>{{ $contactRequest->position_held }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $contactRequest->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $contactRequest->updated_at }}</p>
        </div>
    </div>
</div>
