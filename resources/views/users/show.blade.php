@extends('BackOffice_layouts.master')

@section('title')
    Les Administrateurs | {{ config('app.name') }}
@endsection

@section('content')

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color: white !important">Détails Administrateur</h3>
                </div>
                <div class="col-12 col-md-6 order-md-2 order-first">
                    <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                        <ol class="breadcrumb">
                            <a class="btn btn-danger float-right"href="{{ route('users.index') }}">Retour</a>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="content px-3">
            <div class="card">
                <div class="card-body">
                    @include('users.show_fields')
                </div>

            </div>
        </div>
    </div>
@endsection
