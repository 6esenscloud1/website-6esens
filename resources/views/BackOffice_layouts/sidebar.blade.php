<div class="sidebar-wrapper active">
    <div class="sidebar-header">
        <div class="d-flex justify-content-between">
            <div class="logo">
                <a href="index.html"><img src="{{asset('assets/images/logo.jpg')}}" alt="Logo" srcset=""></a>
            </div>
            <div class="toggler">
                <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
            </div>
        </div>
    </div>
    <div class="sidebar-menu">
        <ul class="menu">
            <li class="sidebar-item {{ Request::is('home*') ? 'active' : '' }}">
                <a href="{{ route('home') }}" class='sidebar-link'>
                    <i class="bi bi-grid-fill"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('projects*') ? 'active' : '' }}">
                <a href="{{ route('projects.index') }}" class='sidebar-link'>
                    <i class="bi bi-stack"></i>
                    <span>Nos Réalisations</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('categoryProjects*') ? 'active' : '' }}">
                <a href="{{ route('categoryProjects.index') }}" class='sidebar-link'>
                    <i class="bi bi-collection-fill"></i>
                    <span>Catégorie Réalisation</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('teams*') ? 'active' : '' }}">
                <a href="{{ route('teams.index') }}" class='sidebar-link'>
                    <i class="bi bi-people-fill"></i>
                    <span>Notre Equipe</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('servicios*') ? 'active' : '' }}">
                <a href="{{ route('servicios.index') }}" class='sidebar-link'>
                    <i class="bi bi-pen-fill"></i>
                    <span>Nos services</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('typeServices*') ? 'active' : '' }}">
                <a href="{{ route('typeServices.index') }}" class='sidebar-link'>
                    <i class="bi bi-pen-fill"></i>
                    <span>Type d'Activations</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('candidacies*') ? 'active' : '' }}">
                <a href="{{ route('candidacies.index') }}" class='sidebar-link'>
                    <i class="bi bi-grid-1x2-fill"></i>
                    <span>Candidature Spontanée</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('newLetters*') ? 'active' : '' }}">
                <a href="{{ route('newLetters.index') }}" class='sidebar-link'>
                    <i class="bi bi-envelope-open-fill"></i>
                    <span>NewsLetters</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('contactRequests*') ? 'active' : '' }}">
                <a href="{{ route('contactRequests.index') }}" class='sidebar-link'>
                    <i class="bi bi-person-lines-fill"></i>
                    <span>Contacts Clients</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('slides*') ? 'active' : '' }}">
                <a href="{{ route('slides.index') }}" class='sidebar-link'>
                    <i class="bi bi-file-slides-fill"></i>
                    <span>Gestion Slides</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('users*') ? 'active' : '' }}">
                <a href="{{ route('users.index') }}" class='sidebar-link'>
                    <i class="bi bi-person-circle"></i>
                    <span>Administrateurs</span>
                </a>
            </li>
        </ul>
    </div>
    <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
</div>
